\documentclass[25pt,a1paper]{tikzposter}
\usepackage{lipsum}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{latexsym}

\usepackage[maxnames=1,style=authoryear]{biblatex}
\addbibresource{jabref.bib}
\AtEveryBibitem{\clearfield{title}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{doi}}{}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{issn}}{}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{isbn}}{}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{url}}{}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{pages}}{}}
%% Tikzposter is highly customizable: please see
%% https://bitbucket.org/surmann/tikzposter/downloads/styleguide.pdf

%% Available themes: see also
%% https://bitbucket.org/surmann/tikzposter/downloads/themes.pdf
\usetheme{Default}
% \usetheme{Rays}
% \usetheme{Basic}
% \usetheme{Simple}
% \usetheme{Envelope}
% \usetheme{Wave}
% \usetheme{Board}
% \usetheme{Autumn}
% \usetheme{Desert}

%% Further changes to the title etc is possible
% \usetitlestyle{Default}
% \usetitlestyle{Basic}
% \usetitlestyle{Empty}
% \usetitlestyle{Filled}
% \usetitlestyle{Envelope}
% \usetitlestyle{Wave}
% \usetitlestyle{verticalShading}

% Colors
\input{resources/colors.tex}
\input{resources/tikz_colors.tex}
\usecolorstyle{MPIEBPale}


\author{Carsten Fortmann-Grote, Paul B. Rainey}
\title{Detection of data manipulation}
%% Optional title graphic
\titlegraphic{\includegraphics[height=5cm]{mpieb_logo_FGwhite_BGalpha}}
%% Uncomment to switch off tikzposter footer
\tikzposterlatexaffectionproofoff


\settitle{%
  \centering
  \hbox{%
    \mbox{}
    \hspace*{9cm}%
    \@titlegraphic\hspace*{\TP@titlegraphictotitledistance}\vbox{%
      \color{titlefgcolor}
      {\bfseries \Huge \sc \@title \par}
      \vspace*{1em}
      {\large \@author \par}%
    }
  }
}
\begin{document}
\maketitle[width=.95\linewidth,titlegraphictotitledistance=-8cm]
\begin{columns}
\column{0.499}
% \block{Abstract}{
%   \noindent
%   \textbf{Falsification}, \textbf{fabrication}, and \textbf{plagiarism} (FFP) of scientific data and
%   results are ubiquitous problems with far reaching consequences for
%   science and society in general. Traditional statistical methods to detect such misbehavior
%   are based on \textbf{digit frequency statistics}, for example Benford's law, or \textbf{statistical tests}.
%   Strictly speaking, these methods are mainly applicable to datasets with
%   a large number of observations spanning multiple orders of magnitude. 
%   As an example, we show that \textbf{RNASeq data} is in excellent agreement with
%   Benford's law of first and second digit frequencies.
%   In addition, we present a method to detect manipulations in data with only \textbf{few
%   observations},
%   based on \textbf{maximum likelihood statistics}.
%   We give preliminary results from a sensitivity analysis using simulated and \textbf{randomly manipulated qPCR} data.
% }%
%
\block{Leading digit methods}{%
  \innerblock{Benford's law\parencite{Benford1938,Fewster2009}}{%
    \begin{itemize}
    \item Benford's law predicts the frequency of 1st (2nd, 3rd, ...) digits in
      data spanning multiple orders of magnitude
    \end{itemize}
    \begin{center}
      \includegraphics[width=.9\linewidth]{benford_lognormal_stripes}
    \end{center}
    \begin{itemize}
    \item digit 1 intervals $[0.1\ldots 0.2)$, $[1\ldots 2)$, $[10\ldots 20)$, \ldots  make up 30\% of a logarithmic scale
    \item[$\Rightarrow$] On average, 30\% of all numbers will have 1st digit 1.
    \end{itemize}
  }
  \innerblock{1st digit frequencies outperform raw data as indicator for manipulated 
    GCNV data \parencite{Bradshaw2021}}{
    \begin{center}
      \begin{minipage}{\colwidth}
        \begin{minipage}{.49\colwidth}
          \textbf{... using 1st digit freq.}\\
          \includegraphics[width=.4\colwidth, clip]{journal.pone.0260395.g003.PNG}
        \end{minipage}
        \hfill
        \begin{minipage}{.49\colwidth}
          \textbf{... using raw data}\\
          \includegraphics[width=.4\colwidth, clip]{journal.pone.0260395.g002.PNG}
        \end{minipage}
      \end{minipage}
    \end{center}
  }
  \innerblock{Application to RNASeq}{%
    Here we analyse the distribution of \textbf{leading digits in RNASeq data} (count
    tables). The results indicate that \textbf{RNASeq data obeyes Benford's law} to a
    very high degree. We conclude that manipulations in RNASeq data can be
    detected by this method.

    % \textbf{Benford analysis over entire dataset (MPGCF Project 4293)}\\
    % \includegraphics[width=.49\colwidth]{rnaseq_all_1stdigit.png}

    \begin{center}
    \includegraphics[width=.9\colwidth, viewport=0 0 1060 455, clip]{rnaseq_4293_benford_samples.pdf}\\
    Evaluation results for different methods of manipulation:
    \includegraphics[width=.9\colwidth]{Screenshot_2023-06-23_15-48-07}
    \end{center}
}%
}
\column{0.499}
\block{Distribution testing
  %for ``small'' datasets $n\simeq \mathcal{O}(1)$}{
  }{
\innerblock{Likelihood method}{%
  \begin{center}
    \includegraphics[width=.49\linewidth]{likelihood_method_flow.pdf}
    \includegraphics[width=.49\linewidth]{qPCRModel_5x5.png}
  \end{center}
}
\innerblock{Baseline model: Poisson dispersion}{%
  \begin{itemize}
  \item Poisson distribution:
    $ \text{dispersion} = \frac{\text{var}}{\text{mean}} \equiv 1 $

  \item $\frac{\text{var}}{\text{mean}} \gg 1, \ll 1$ indicates 
  ``... fixed mean but forgot variance.''
\end{itemize}

 Dispersion (left) and predictions of manipulation (right)
  \begin{center}
   \includegraphics[width=.49\linewidth]{Poisson_dispersion_20x20}
   \includegraphics[width=.49\linewidth]{poisson_predictions_20x20}
  \end{center}
}%
\innerblock{Evaluation likelihood method vs. Poisson dispersion}{%
  \begin{center}
    \includegraphics[width=\linewidth]{likelihood_vs_poissondispersion.pdf}
  \end{center}
}
}
\block{Conclusions, Outlook}{
  \begin{itemize}
    \item Benford's law is perfectly capable of identifying manipulations in
      large scale RNASeq data.
    \item Both, likelihood method and Poisson dispersion, have a high sensitivity
      to manipulations in ``small'' count data (hits are likely to be true).
  \item Likelihood method 
    is outperformed by dispersion testing on raw data.
  \item More extensive testing is needed (enlarged test dataset, multiple test
    evaluations) to confirm our result.
  \end{itemize}
}
% \printbibliography[heading=none]
\end{columns}
\node at (-.3\linewidth,-.48\textheight) {\tiny\includegraphics[width=2cm]{by}%
  \tiny\ This work  is licensed under a Creative Commons Attribution 4.0 International License.};
% \block{Some numbers on FFP\parencite{Oransky2022,Freitas2021,Allen2016}}{%

%   \begin{itemize}
%   \item Retractions (retractionwatch.com) increased from 45/month in 2016 to $>$300/month in 2021
%   \item 45\% of retractions in 2014 due to scientific misconduct
%   \item 67.4\% of 2047 retractions in PubMed indexed journals due to
%     misconduct, 43.4\% identified or suspected as fraudulent
%   \item 15\% of UC graduate students would be willing to select, omit, fabricate data to win a grant or to publish a paper
%   \item  7\% of UC PostDocs are willing to select or omit data to improve their results
%   \item  27\% of AAAS members found or witnessed research was fabricated, falsified, or plagiarized
%   \end{itemize}
% }
\end{document}
