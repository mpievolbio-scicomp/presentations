#+OPTIONS: num:nil toc:nil
#+REVEAL_TRANS: None
#+REVEAL_THEME: Solarized
#+Title: Linked (Open) Data for Microbial Population Biology
#+shorttitle: Linked (Open) Data for MPB
#+Author: Carsten Fortmann-Grote
#+Email: carsten.fortmann-grote@evolbio.mpg.de
#+Mastodon: @c4r510
#+DATE:      March 12 2024
#+DESCRIPTION: 
#+KEYWORDS: NFDI, Bio Imaging, Linked Data, Controlled Vocabulary, Knowledge Base
#+LANGUAGE:  en
#+OPTIONS:   H:1 num:t toc:nil \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t email:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:https://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+HTML_LINK_UP:
#+HTML_LINK_HOME:
#+startup: beamer
#+LaTeX_CLASS: beamer
#+BEAMER_FRAME_LEVEL: 2
#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)
#+BEAMER_HEADER: \titlegraphic{\includegraphics[height=0.3\textheight]{~/Pictures/MPIEB/Luftbild.jpg}}
#+latex_header: \mode<beamer>{\usetheme{Madrid}}
#+latex_header: \usepackage{biblatex}
#+latex_header: \usepackage{tikz}
#+latex_header: \usetikzlibrary{shapes.geometric}
#+latex_header: \usetikzlibrary{positioning}
#+latex_header: \usetikzlibrary{calc}
#+latex_header: \usetikzlibrary{arrows}

** A short tour of the institute                                    :B_frame:
\tiny
:PROPERTIES:
:BEAMER_env: frame
:END:
*** Evolutionary Genetics (D. Tautz (Emeritus))
:PROPERTIES:
:BEAMER_env: block
:BEAMER_opt: t
:BEAMER_col: 0.3
:END:
- Model organisms: Mus domesticus, mus musculus
- Behavioural genomics
- Population genetics
[[file:Pictures/screenshots/2023-06-19_09-46-11_screenshot.png]]
credits: https://www.evolbio.mpg.de/3039130/group_evolanimalbehpers

*** Microbial Population Biology (P. Rainey)
:PROPERTIES:
:BEAMER_COL: 0.3
:BEAMER_opt: t
:BEAMER_env: block
:END:
- Model organism: Pseudomonas fluorescens, Bacillus sub.
- Evolution of communities
- Host-microbe interactions
- Genetics
[[~/Pictures/MPIEB/sbw25.png]]
credits: Theodosiou (left), Schwarz (middle), Grote (right)

*** Theoretical Biology (A. Traulsen)
:PROPERTIES:
:BEAMER_opt: t
:BEAMER_COL: 0.3
:BEAMER_env: block
:END:

- Population Structure and Game Theory
- Metaorganisms
- Cancer Evolution
[[file:Pictures/screenshots/2023-06-19_10-07-46_screenshot.png]]
Hindersin et al, PLOS Comp. Bio (2019) DOI:10.1371/journal.pcbi.1004437


* Pseudomonas fluorescens SBW25 is a model organism for Microbial Population Biology
** Research topics in Microbial Population Biology                  :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:
- Bacterial communities as Darwinian entities
- Antibiotica resistance
- Bacteria and phages
- Host-microbe interactions (plants, soil)
- Long term evolution experiments
- Adaptation to artificial selection pressures
- Genetic bases for evolutionary processes
** A typical MPB project                                            :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:
- Wildtype clone and/or genetically modified strain(s)
- Timelapse microscopy from growing microbial communities
- Time resolved whole genome / core gene NGS data
- Transcription profiles
- Optical density measurements from plaque assays
- Functional annotations
**Need for integrated analysis of multiomics-multimodal data**

"Find images, ELN entries for \Delta mreB strains and annotations for mreB"


** Integrating data the sparqling way
:PROPERTIES:
:BEAMER_env: frame
:END:
***                                                                 :B_block:
[[/home/grotec/Repositories/presentations/tex/drawings/PfluKnowledgeGraph_20240312.png]]

# :PROPERTIES:
# :BEAMER_env: block
# :END:
# #+begin_export latex
# \begin{center}
#   \begin{tikzpicture}
#      \node[rectangle, draw, fill=blue!30] (editor) at (0, 0) {SPARQL Query Editor};
#      \node[rectangle, draw] (genome) at (-2, -2) {Genome RDF};
#      \node[rectangle, draw] (omero) at (2, -2) {OMERO RDF};
#      \node[rectangle, draw] (strainrdf) at (-2, 2) {Strains RDF};
#      \node[rectangle, draw] (obis) at (2, 2) {OpenBIS RDF};
     
#      \node[rectangle, draw, left of=genome, xshift=-2cm] (genomedb) {Genome RDB};
#      \node[rectangle, draw, right of=omero, xshift=2cm] (omerodb)  {OMERO RDB};
#      \node[rectangle, draw, left of=strainrdf, xshift=-2cm] (straindb) {Strains RDB};
#      \node[rectangle, draw, right of=obis, xshift=2cm] (obisds)  {OpenBIS RDB};
#      \draw[<->] (editor) -- (genome);
#      \draw[<->] (editor) -- (omero);
#      \draw[<->] (editor) -- (strainrdf);
#      \draw[<->] (editor) -- (obis);
#      \draw[<->] (genome) -- (genomedb);
#      \draw[<->] (omero) -- (omerodb);
#      \draw[<->] (strainrdf) -- (straindb);
#      \draw[<->] (obis) -- (obisds);
#     \end{tikzpicture}
# \end{center}
# #+end_export
** Data integration of internal sources                             :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:
| DB               | API     | LOD ready? | RDF conversion                 |
|------------------+---------+------------+--------------------------------|
|                  |         |            |                                |
| Tripal Genome DB | JSON-LD | yes        | SPARQL                         |
|                  |         |            | SPARQL-anything                |
|                  |         |            | virtualization                 |
|                  |         |            | *JSON to RDF serialization*    |
| OMERO            | JSON-LD | yes        | omero-rdf                      |
| OpenBIS          | JSON    | ???        | ??? ("semantic annotation")    |
| StrainDB         | None    | no         | csv dump $\rightarrow$ csv2rdf |

** CSV2RDF                                                          :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:
[[file:/home/grotec/Pictures/varia/FW44NXZX0AAFTzg.jpeg]]
\tiny Credits: Sarah Komla-Ebri

** Genome database
:PROPERTIES:
:BEAMER_env: frame
:END:
*** P.flu SBW25 Genome Database
:PROPERTIES:
:BEAMER_opt: t
:BEAMER_COL: 0.49
:BEAMER_env: block
:END:
[[file:~/Pictures/Screenshots/Screenshot_2022-06-08_12-41-18.png]]
[[file:~/Pictures/Screenshots/Screenshot_2022-07-05_21-58-08.png]]
*** Organism Database (Tripal)
:PROPERTIES:
:beamer_opt: t
:beamer_col: 0.49
:BEAMER_env: block
:END:
- Based on Drupal CMS
- Implements Chado DB Scheme (SQL)
- Converts genome annotation into searchable, cross-linked database
- Integrates Genome Browser (JBrowse)
- Supports OBO Ontologies & Controlled Vocabularies (Sequence Ontology, Gene Ontology, ...)
- JSON-LD API $\Rightarrow$ **In principle** ready for consumption by LOD clients

** Querying a JSON-LD API                                           :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:beamer_opt: fragile
:END:
*** Direct query                                                  :B_column:
:PROPERTIES:
:BEAMER_env: block
:END:

\tiny
#+begin_example sparql :url http://141.5.101.176:3030/PFLUKG/sparql :format text/csv
  SELECT distinct ?s ?p ?o where 
    {
      ?s a SO:0000316 .
      ?s ?p ?o .
  }
#+end_example

#+begin_export latex
\begin{verbatim}
| s         | p                                               | o                                  |
|-----------+-------------------------------------------------+------------------------------------|
| CDS:11845 | http://www.w3.org/1999/02/22-rdf-syntax-ns#type | so:0000316                         |
| CDS:11845 | http://www.w3.org/2000/01/rdf-schema#label      | CDS:PFLU_0001-0                    |
| CDS:11845 | http://edamontology.org/data_2091               | 11845                              |
| CDS:11845 | https://schema.org/name                         | CDS:PFLU_0001-0                    |
| CDS:11845 | http://edamontology.org/data_0842               | CDS:PFLU_0001-0                    |
| CDS:11845 | http://purl.obolibrary.org/obo/OBI_0100026      | Pseudomonas fluorescens            |
| CDS:11845 | http://www.w3.org/2000/01/rdf-schema#type       | CDS                                |
| CDS:11845 | http://edamontology.org/data_2012               | CDS:11845/Sequence+coordinates     |
| CDS:11845 | http://purl.obolibrary.org/obo/OGI_0000021      | CDS:11845/location+on+map          |
| CDS:11845 | http://purl.obolibrary.org/obo/SBO_0000374      | CDS:11845/relationship             |
| CDS:11845 | http://purl.obolibrary.org/obo/SBO_0000554      | CDS:11845/database+cross+reference |
| CDS:11845 | http://semanticscience.org/resource/SIO_001166  | CDS:11845/annotation               |
\end{verbatim}
#+end_export

***                                                            :B_alertblock:
:PROPERTIES:
:BEAMER_env: alertblock
:END:
Object URIs are Graph URIs of linked json-ld documents.
Exploring the graph across documents requires dynamic generation of graph URIs, not supported in SPARQL1.1

** Querying a JSON-LD API (cont.)                                   :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:beamer_opt: fragile
:END:
*** SPARQL-Anything                                                :B_column:
:PROPERTIES:
:BEAMER_env: block
:END:
\tiny
#+begin_src sparql :url http://141.5.101.176:3000/sparql.anything :format text/csv :async
  PREFIX fx: <http://sparql.xyz/facade-x/ns/>
  PREFIX xyz: <http://sparql.xyz/facade-x/data/>
  PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
  SELECT distinct ?gene_name ?go  where {
    service <x-sparql-anything:>
    {
      fx:properties fx:location "http://pflu.evolbio.mpg.de/web-services/content/v0.1/CDS/12135";
                    fx:media-type "application/ld+json" .
      ?s a <http://www.sequenceontology.org/browser/current_svn/term/SO:0000316> .
      ?gs <http://pflu.evolbio.mpg.de/cv/lookup/local/gene> ?gene_name .
      ?gs <http://semanticscience.org/resource/SIO_001166> ?annotation .
    }
  optional {
    service <x-sparql-anything:>
    {
      fx:properties fx:location ?annotation ;
      fx:media-type "application/ld+json" .
      ?annotation_s <http://semanticscience.org/resource/SIO_001080> ?vocab ;
                    <http://edamontology.org/data_2091> ?go_term ;
                    <https://schema.org/name> ?go_name ;
                    <http://purl.obolibrary.org/obo/IAO_0000115> ?definition .
      bind(concat(concat(?vocab, ":"), ?go_term) as ?go)
    }
  }
  }
#+end_src

** Querying a JSON-LD API                                           :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:beamer_opt: fragile
:END:
\tiny
\begin{verbatim}
| gene_name | go         | go_name                           |
|-----------+------------+-----------------------------------|
| wssH      | GO:0043806 | keto acid formate lyase activity  |
| wssH      | GO:0005886 | plasma membrane                   |
| wssH      | GO:0016021 | integral component of membrane    |
| wssH      | GO:0016746 | acyltransferase activity          |
| wssH      | GO:0042121 | alginic acid biosynthetic process |
\end{verbatim}
***                                                            :B_alertblock:
:PROPERTIES:
:BEAMER_env: alertblock
:END:
Still, this is quite complicated and probably not well supported by SPARQL query editors

** Virtualization                                                   :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:beamer_opt: fragile
:END:
#+attr_latex: :width .6\textwidth
[[/home/grotec/Pictures/VKG_schema.png]]
\tiny{ Xiao et al (2019)}
** Example: VKG for the P.flu SBW25 Genome database                 :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:beamer_opt: fragile
:END:
*** Step 1: Bootstrapping with ontop-vkg                            :B_block:
:PROPERTIES:
:BEAMER_env: block
:END:
\tiny

#+begin_example shell
$> ontop bootstrap --db-url URL -p PROPS -t ONTOLOGY -m MAPPING
#+end_example
#+begin_src sparql
 <http://www.semanticweb.org/grotec/ontologies/2022/5/tripalv3/gene#dbxref_id> rdf:type owl:DatatypeProperty .
 : mappingId	MAPPING-ID793
 : target		_:ontop-bnode-534{feature_id}/{dbxref_id}/{organism_id}/{name}/{uniquename}/{residues}/{seqlen}/{md5checksum}/{type_id}/{is_analysis}/{is_obsolete}/{timeaccessioned}/{timelastmodified} a g:protein_coding_gene ;
 g:protein_coding_gene#feature_id {feature_id}^^xsd:integer ;
 g:protein_coding_gene#dbxref_id {dbxref_id}^^xsd:integer ;
 g:protein_coding_gene#organism_id {organism_id}^^xsd:integer ;
 g:protein_coding_gene#name {name}^^xsd:string ;
 g:protein_coding_gene#uniquename {uniquename}^^xsd:string ;
 g:protein_coding_gene#residues {residues}^^xsd:string ;
 g:protein_coding_gene#seqlen {seqlen}^^xsd:integer ;
 g:protein_coding_gene#md5checksum {md5checksum}^^xsd:string ;
 g:protein_coding_gene#type_id {type_id}^^xsd:integer ;
 g:protein_coding_gene#is_analysis {is_analysis}^^xsd:boolean ;
 g:protein_coding_gene#is_obsolete {is_obsolete}^^xsd:boolean ;
 g:protein_coding_gene#timeaccessioned {timeaccessioned}^^xsd:dateTime ;
 g:protein_coding_gene#timelastmodified {timelastmodified}^^xsd:dateTime . 
 : source		SELECT * FROM "chado"."protein_coding_gene"
#+end_src

** Example: VKG for the P.flu SBW25 Genome database                 :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:beamer_opt: fragile
:END:
*** Step 2: Cleanup                                                 :B_block:
:PROPERTIES:
:BEAMER_env: block
:END:
  - Highly redundant ontology: Every column label is translated into a data property **for every table it occurs in**

#+begin_example sparql
<tripalv3:gene#dbxref_id> rdf:type owl:DatatypeProperty .
<tripalv3:non_protein_coding#dbxref_id> rdf:type owl:DatatypeProperty .
#+end_example

***                                                                 :B_frame:
:PROPERTIES:
:BEAMER_env: alertblock
:END:
- Define single property to be applied across all tables.
- Virtualization eventually worked but query execution time is slooooooooooooooooowwwwww

** Flattening a deeply nested JSON-LD graph                         :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:

*** github.com/mpievolbio-scicomp/pytripalserializer
:PROPERTIES:
:beamer_col: 0.48
:END:
#+attr_latex: :width .8\textwidth

[[file:Pseudomonas_fluorescens_SBW25_is_a_model_organism_for_Microbial_Population_Biology/2024-03-11_14-32-18_screenshot.png]]

***                                                                :B_column:
:PROPERTIES:
:BEAMER_env: block
:beamer_col: .48
:END:
- rdflib for loading json-ld into graph structure
- iteratively parses linked graph uris
- dask for distributed computing
- serialized to turtle format
- -> 2.1 million Triples in ca. 12hrs

** Our P.flu SBW25 RDF playground                                   :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:


***                                                                :B_column:
:PROPERTIES:
:beamer_col: .48
:END:
#+attr_latex: :height .3\textheight
[[file:Pseudomonas_fluorescens_SBW25_is_a_model_organism_for_Microbial_Population_Biology/2024-03-12_10-12-20_screenshot.png]]

#+attr_latex: :height .3\textheight
[[file:Pseudomonas_fluorescens_SBW25_is_a_model_organism_for_Microbial_Population_Biology/2024-03-12_10-13-27_screenshot.png]]

***                                                                :B_column:
:PROPERTIES:
:beamer_col: .48
:END:
#+attr_latex: :height .3\textheight
[[file:Pseudomonas_fluorescens_SBW25_is_a_model_organism_for_Microbial_Population_Biology/2024-03-12_10-17-30_screenshot.png]]

* Example queries

** Image, genotype, mpb number and straindb columns                 :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:beamer_opt: fragile
:END:
\tiny
#+begin_src sparql :url http://micropop046.evolbio.mpg.de:3030/omero-rdf-playground/sparql :format text/csv :async

  prefix ome_core: <http://www.openmicroscopy.org/rdf/2016-06/ome_core/>
  prefix mpimap: <http://ome.evolbio.mpg.de/MapAnnotation/>
  prefix mpiimg: <http://ome.evolbio.mpg.de/Image/>
  prefix mpi: <http://ome.evolbio.mpg.de/>
  prefix strains: <http://raineylab.evolbio.mpg.de/straindb/strains/>
  prefix props: <http://raineylab.evolbio.mpg.de/straindb/properties/>
    select distinct ?img ?mpbn ?genotype ?phenotype where {
      ?img <http://www.wikidata.org/prop/direct/P180> ?ann .
      ?ann ome_core:Map ?map . 
      ?map ome_core:Key ?key .
      ?map ome_core:Value ?mpb .
      filter(?key in ("", "MPB")).

      bind(concat("MPB", ?mpb) as ?mpbn ).
    service  <http://micropop046.evolbio.mpg.de:3030/MPBStrainDB/sparql> {
        ?strain props:sId ?mpbn .
        ?strain props:genotype ?genotype .
        ?strain props:phenotype ?phenotype .
    }
           }
  order by ?img
  limit 7
#+end_src


| img                                      | mpbn     | genotype                             | phenotype        |
|------------------------------------------+----------+--------------------------------------+------------------|
| http://ome.evolbio.mpg.de/Image/6752778  | MPB15447 | PFLU0085 del 1309-1374 (del 437-458) | Wrinkly Spreader |
| http://ome.evolbio.mpg.de/Image/6752779  | MPB15447 | PFLU0085 del 1309-1374 (del 437-458) | Wrinkly Spreader |
| http://ome.evolbio.mpg.de/Image/6752780  | MPB15447 | PFLU0085 del 1309-1374 (del 437-458) | Wrinkly Spreader |
| http://ome.evolbio.mpg.de/Image/6752781  | MPB15456 | WsT AwsX delta229-261                | Wrinkly Spreader |
| http://ome.evolbio.mpg.de/Image/6752782  | MPB15456 | WsT AwsX delta229-261                | Wrinkly Spreader |
| http://ome.evolbio.mpg.de/Image/6752783  | MPB15456 | WsT AwsX delta229-261                | Wrinkly Spreader |
| https://ome.evolbio.mpg.de/Image/6752855 | MPB15455 | “22A1” WspF 150bpdel (delta 677-826) | Wrinkly Spreader |

* GUI SPARQL clients
** SparNatural for visual query generation                          :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:beamer_opt: fragile
:END:
[[file:~/Sandbox/pflukg/index.html]]

- Graphical query editor
- Supports federation and customized data sources
- No support (yet) for BIND and aggregations

* The role of metadata standardization
** On omero-rdf                                                     :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:beamer_opt: fragile
:END:
\tiny
#+begin_src sparql :url http://micropop046.evolbio.mpg.de:3030/omero-rdf-playground/sparql :format text/csv :async
    select distinct ?key ?val where {
      # ?ann <http://www.wikidata.org/prop/direct/P180> ?img .
      ?ann ome_core:Map ?map . 
      ?map ome_core:Key ?key .
      ?map ome_core:Value ?val .
      }
#+end_src

| key                            | val                                            |
|--------------------------------+------------------------------------------------|
| Type                           | still                                          |
| Organism                       | Pseudomonas fluorescens                        |
| Strain                         | SBW25                                          |
| MPB                            | 15447                                          |
| Parent                         | 0                                              |
| Investigation                  | Deep Learning for MPB                          |
| Study                          | Segmentation and Classif...5 colony morphotype |
| Assay                          | 0085                                           |
| Phenotype                      | WS                                             |
| Genotype                       | 0085                                           |


***                                                                 :B_frame:
:PROPERTIES:
:BEAMER_env: block
:END:
- omero-rdf stores key, value as bnode property values.
- would need key as property, value as property value
- requires loading ontologies, controlled vocabs into omero and exposure in map annotations.



* Tasks and contributions to NFDI4BI
** Tasks and contributions to NFDI4BI                               :B_frame:
:PROPERTIES:
:BEAMER_env: frame
:END:
- P.flu SBW25 Knowledge Graph as a Use Case
- Controlled vocabularies for microbial population biology and evolutionary cell biology
- Contributions to linked open data software, clients, recommendations

