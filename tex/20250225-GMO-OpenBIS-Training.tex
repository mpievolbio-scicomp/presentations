% Created 2025-02-24 Mon 15:24
% Intended LaTeX compiler: pdflatex
\documentclass[presentation]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\mode<beamer>{\usetheme{Madrid}}
\usepackage{biblatex}
\usepackage{tikz}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{positioning}
\usetikzlibrary{calc}
\usetikzlibrary{arrows}
\input{resources/colors}
\definecolor{mpievolbio_green}{RGB}{0,124,119}
\setbeamercolor*{frametitle}{fg=white,bg=MPGGreen}
\setbeamercolor*{section in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor*{date in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor*{author in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor*{title in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor{structure}{bg=MPGGreen}
\setbeamercolor{block title}{fg=white,bg=structure.bg}
\setbeamercolor{block body}{use=block title,bg=block title.bg!10}
\setbeamercolor{title}{fg=white,bg=MPGGreen}
\setbeamercolor{itemize item}{fg=MPGGreen}
\setbeamercolor{itemize subitem}{fg=MPGGreen}
\setbeamercolor{enumerate item}{fg=MPGGreen}
\setbeamercolor{enumerate subitem}{fg=MPGGreen}
\setbeamercolor{description item}{fg=MPGGreen}
\setbeamertemplate{itemize item}[circle]
\setbeamertemplate{itemize subitem}[circle]
\setbeamertemplate{itemize subsubitem}[circle]
\setbeamercolor{alerted text}{fg=MPGOrange,bg=MPGLightGreen}
\usetheme{default}
\author{Carsten Fortmann-Grote\thanks{carsten.fortmann-grote@evolbio.mpg.de}}
\date{2025-02-25}
\title{Documentation of GMOs in OpenBIS}
\titlegraphic{\includegraphics[height=0.3\textheight]{~/Pictures/MPIEB/Luftbild.jpg}}
\logo{\includegraphics[width=0.05\textwidth]{CC-BY-4.0_88x31.png}\hspace{.02\textwidth}\hspace{.02\textwidth}\includegraphics[width=0.05\textwidth]{mpg_logo_FGgreen_BGalpha}}
\hypersetup{
 pdfauthor={Carsten Fortmann-Grote},
 pdftitle={Documentation of GMOs in OpenBIS},
 pdfkeywords={eln, lims, openBIS, genetically modified organisms, mpi, evolutionary biology, training, technical staff},
 pdfsubject={Presentation for the OpenBIS training on documentation of GMOs in OpenBIS},
 pdfcreator={Emacs 28.2 (Org mode 9.7.4)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section{Outline}
\label{sec:org8a7d423}
\begin{frame}[label={sec:org2794ab3}]{Outline}
\begin{alertblock}{Questions}
\begin{itemize}
\item Where are GMOs registered in OpenBIS?
\item What are the required data about GMOs?
\item How can I create/edit/delete GMOs?
\item How does batch creation/editing work?
\end{itemize}
\end{alertblock}
\begin{alertblock}{Objectives}
\begin{itemize}
\item Locate GMOs and their Projects in OpenBIS
\item Understand GMOs entries and their fields
\item Master the process of creating and editing GMO entries in OpenBIS
\item Create a printable view of a GMO project
\end{itemize}
\end{alertblock}
\end{frame}
\section{GMO Projects}
\label{sec:orgd3c51cf}
\begin{frame}[label={sec:orgbf250ed}]{GMO Projects in OpenBIS}
\begin{block}{GMO Materials space holds the 6 registered GMO Projects}
\begin{center}
\includegraphics[height=.7\textheight]{img/GMO_Projects/2025-02-24_15-16-01_screenshot.png}
\end{center}
\end{block}
\end{frame}
\begin{frame}[label={sec:org3c9befb}]{Example: GMO Pseudomonas fluorescens SBW25}
\begin{block}{MPB GMOs in OpenBIS}
\begin{center}
\includegraphics[width=.9\linewidth]{img/GMO_Projects/2025-02-24_10-59-14_screenshot.png}
\end{center}
\end{block}
\end{frame}
\begin{frame}[label={sec:orge50f517}]{GMO properties (column headers)}
\begin{itemize}
\item \alert{Name} (unique in the table)
\item \alert{Donor} name \& risk group
\item \alert{Recipient} name \& risk group
\item \alert{Vector} name
\item \alert{Nucleic acid} name, risk group \& purification level
\item \alert{GMO description}, risk group
\item \alert{Creation} date (\& disposal date)
\item \alert{Date \& Signature} of responsible person
\end{itemize}
\end{frame}
\section{Single GMO entry creation}
\label{sec:org3e29e4c}
\begin{frame}[label={sec:orgcf2989b}]{Creation of single GMO entry}
\begin{block}{Gmo Materials -> Your Project -> Your Collection -> New Formblattz}
\begin{center}
\includegraphics[width=.8\textwidth]{img/GMO_Projects/2025-02-24_13-21-34_screenshot.png}
\end{center}
\end{block}
\end{frame}
\begin{frame}[label={sec:orge897d22}]{Creation of single GMO entry}
\begin{block}{Enter GMO details into form (don't edit "Code")}
\begin{center}
\includegraphics[width=.8\textwidth]{img/GMO_Projects/2025-02-24_13-29-40_screenshot.png}
\end{center}
\end{block}
\end{frame}
\begin{frame}[label={sec:org17f0efe}]{Exercise}
\begin{block}{Create a single GMO in the GMO Training Project}
\begin{itemize}
\item Select the Training GMOs Collection
\item Click "New Formblattz"
\item Leave "Code" as is
\item Enter fields with (*)
\item Use your groups name in "Project" field
\item Click Save
\item Locate your new entry in the Training GMOs Collection
\end{itemize}
\end{block}
\end{frame}
\section{Batch GMO creation}
\label{sec:orgae06d93}
\begin{frame}[label={sec:orgab61afa}]{Batch creation of GMOs}
\begin{block}{Use Excel spreadsheet (XLS) for batch registration}
\begin{center}
\includegraphics[width=.8\textwidth]{img/GMO_Projects/2025-02-24_13-39-43_screenshot.png}
\end{center}
\end{block}
\end{frame}
\begin{frame}[label={sec:org2bf6b5c}]{Batch creation of GMOs}
\begin{block}{Download template spreadsheet}
\begin{center}
\includegraphics[width=.8\textwidth]{img/GMO_Projects/2025-02-24_13-44-10_screenshot.png}
\end{center}
\end{block}
\end{frame}
\begin{frame}[label={sec:orga0a4606}]{Batch creation of GMOs}
\begin{block}{Enter GMOs in template spreadsheet}
\begin{center}
\includegraphics[width=.8\textwidth]{img/GMO_Projects/2025-02-24_13-50-32_screenshot.png}
\end{center}
\end{block}
\end{frame}
\begin{frame}[label={sec:orgc756410}]{Batch creation of GMOs}
\begin{block}{Enter GMOs in template spreadsheet}
\begin{center}
\includegraphics[width=.8\textwidth]{img/GMO_Projects/2025-02-24_13-57-51_screenshot.png}
\end{center}
\end{block}
\end{frame}
\begin{frame}[label={sec:orgf2b1ac4}]{Batch creation of GMOs}
\begin{block}{Locate xlsx spreadsheet in Finder (Mac)/Explorer (Win)/Files (Linux)}
\begin{center}
\includegraphics[width=.8\textwidth]{img/GMO_Projects/2025-02-24_14-06-34_screenshot.png}
\end{center}
\end{block}
\end{frame}
\begin{frame}[label={sec:orgcc6dd30}]{Batch creation of GMOs}
\begin{block}{Select file and click "Accept"}
\begin{center}
\includegraphics[width=.9\linewidth]{img/GMO_Projects/2025-02-24_14-07-40_screenshot.png}
\end{center}
\end{block}
\end{frame}
\begin{frame}[label={sec:orga2b7c8a}]{Batch creation of GMOs}
\begin{block}{Confirm newly registered entries}
\begin{center}
\includegraphics[width=.9\linewidth]{img/GMO_Projects/2025-02-24_14-08-40_screenshot.png}
\end{center}
\end{block}
\end{frame}
\begin{frame}[label={sec:org0b03629}]{Exercise}
\begin{block}{Register 10 GMOs in one batch}
\begin{itemize}
\item Download the template
\item Enter 10 made up GMOs
\item Upload the spreadsheet
\item Locate your entries in the GMO Training Collection
\item Filter the "Registrator" column to show only your entries
\end{itemize}
\end{block}
\end{frame}
\section{Table export}
\label{sec:org08f588c}
\begin{frame}[label={sec:org324871f}]{Export table}
\begin{block}{Select and order columns to export}
\begin{center}
\includegraphics[width=.8\textwidth]{img/Table_export/2025-02-24_14-44-35_screenshot.png}
\end{center}
\end{block}
\end{frame}
\begin{frame}[label={sec:org5380ffd}]{Export table}
\begin{block}{(De)select columns \ldots{}}
\begin{center}
\includegraphics[width=.8\textwidth]{img/Table_export/2025-02-24_15-03-24_screenshot.png}
\end{center}
\end{block}
\end{frame}
\begin{frame}[label={sec:orgf92a352}]{Export table}
\begin{block}{Reorder columns if needed}
\begin{center}
\includegraphics[height=.5\textheight]{img/Table_export/2025-02-24_14-55-38_screenshot.png}
\end{center}
\end{block}
\end{frame}
\begin{frame}[label={sec:org742b299}]{Export table}
\begin{block}{Generate spreadsheet from selected entries and columns}
\begin{center}
\includegraphics[width=.8\textwidth]{img/Table_export/2025-02-24_15-06-32_screenshot.png}
\end{center}
\end{block}
\end{frame}
\section{Summary}
\label{sec:org747344e}
\begin{frame}[label={sec:orgb2e55fb}]{Summary}
\begin{alertblock}{What we have learned:}
\begin{itemize}
\item GMOs are collected in one of six GMO Projects
\item GMOs are entered one-by-one or via batch registration
\item Users can export a formatted GMO table for printing and reporting
\item Users cannot delete GMOs
\end{itemize}
\end{alertblock}
\end{frame}
\end{document}
