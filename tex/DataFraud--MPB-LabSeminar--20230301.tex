% Created 2023-02-07 Tue 10:34
% Intended LaTeX compiler: pdflatex
\documentclass[bigger]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{biblatex}
\usepackage{hyperxmp}
\addbibresource{jabref.bib}
\mode<beamer>{\usetheme{Madrid}}
\usetheme{default}
\author{\uline{Carsten Fortmann-Grote}}
\date{MPB Lab Seminar\\ February 8. 2023}
\title{Detecting Fabricated Data}
\hypersetup{
 pdfauthor={Carsten Fortmann-Grote},
 pdftitle={Detecting Fabricated Data},
 pdfkeywords={Benford's law, good scientific practise, Bayesian statistics, RNASeq},
 pdfsubject={MPB Department Seminar},
 pdfcreator={Emacs 27.1 (Org mode 9.6.1)},
 pdfdate={D:20230301090000Z},
 pdflang={English}
}

\renewcommand{\footnotesize}{\scriptsize}
\begin{document}

\maketitle
\begin{frame}{Outline}
  \tableofcontents
\end{frame}

\section{Introduction}
\label{sec:org1a5a8b5}
\begin{frame}[label={sec:orge993a81}]{FFP: Fabrication, Falsification, and Plagiarism}

  \begin{block}{Fabrication}
    ``making up data or results and recording
    or reporting them''
  \end{block}
  \begin{block}{Falsification}
    ``manipulating research mate-
    rials, equipment, or processes, or changing or omitting data or
    results such that the research is not accurately represented in
    the research record''
  \end{block}
  \begin{block}{Plagiarism}
    ``the appropriation of
    another person’s ideas, processes, results, or words without
    giving appropriate credit''
  \end{block}
  % 
  {\footnotesize
  Definitions according to NSF Office of Science and Technology Policy (
  OSTP) as cited by \fullcite{Resnik2013}.
  }
  
\end{frame}
\begin{frame}
  \frametitle{Some numbers on scientific misconduct}
  \begin{itemize}
  \item Retractions (retractionwatch.com) increased from 45/month in 2016 to $>$300/month in 2021 \footfullcite{Oransky2022}
  \item 45\% of retractions in 2014 due to scientific misconduct \footfullcite{Freitas2021}
    \item 67.4\% of 2047 retractions in PubMed indexed journals due to
      misconduct, 43.4\% identified or suspected as fraudulent \footfullcite{Allen2016}.
  \end{itemize}
\end{frame}
%
\begin{frame}[label={sec:orgba1ca38}]{Survey's display a frightening willingness to fake data\footfullcite{Freitas2021}}
  \begin{block}{UC Grad Students}
    15\% would be willing to select, omit, fabricate data to win a grant or to publish a paper
  \end{block}
  \begin{block}{UC PostDocs}
    7\% willing to select or omit data to improve their results
  \end{block}
  \begin{block}{AAAS member survey}
    27\% found or witnessed research was fabricated, falsified, or plagiarized
  \end{block}
\end{frame}
%
\begin{frame}
  \frametitle{Consequences of FFP in Science\footfullcite{Freitas2021}}
  \begin{itemize}
  \item Damage career of co-authoring colleagues and students
  \item Slowing down scientific progress (following false avenues)
  \item Harmful for patients (clinical studies)
  \item Damage reputation and image of scientific field and science in general
  \end{itemize}
\end{frame}
%
\begin{frame}[label={sec:orge86b169}]{Detecting Fabricated Data}
  \begin{itemize}
  \item Indications for fraud must always be treated with uttermost care and
    discretion.
  \item Not touched upon: Image data fraud, Deep fakes (incl. ChatGPT), \ldots
  \end{itemize}
  \end{frame}
  \begin{frame}{Some context}
  \begin{block}{}
    Having to detect scientific fraud essentially means, the damage has been
    done. How can we (as individuals (staff, supervisors, students, post--docs))
    and as collectives (groups, departments, institutes, society) create a
    working atmosphere that minimizes the perceived or actual factors that
    motivate fraud?
    \begin{itemize}
    \item Pressure to publish \& academic performance indicators
    \item Negative results ``not publishable''
    \item Need to present complete stories (even in lab seminars)
    \end{itemize}
  \end{block}
\end{frame}
%
\section{Detection methods}
\begin{frame}[label={sec:orgeb4418f}]{Detection of falsified and/or fabricated data}
  \begin{block}{Digit preference methods}
    \begin{itemize}
    \item Leading digits frequencies (data spans multiple orders of magnitude)
    \item Last digit frequencies (mainly human recorded data)
    \end{itemize}
  \end{block}
  \begin{block}{Statistical methods}
    \begin{itemize}
    \item Variance checks (manipulate means but not variance)
    \item Distribution checks (distribution known/assumable?)
    \end{itemize}
  \end{block}
\end{frame}
\begin{frame}[label={sec:org6c286e6}]{Which data is fake?}
  \begin{figure}[htbp]
    \centering
    \includegraphics[height=.7\textheight]{2023-01-27_11-43-33_screenshot.png}
    {\footnotesize%
      \caption{Reproduced from \fullcite{Fewster2009}}
    }
  \end{figure}
\end{frame}

\section{Digit preference methods: Benford's law}
\label{sec:org41e7354}
\begin{frame}[label={sec:orgd90ee91}]{Benford's law test}
  Benford's law, also known as "first digit law", states that the leading
  digit in numerical datasets is distributed as

  \begin{block}{}
    \begin{equation*}
      p(D_1=d) = \log_{10} \left(1 + 1/d \right)
    \end{equation*}
  \end{block}{}
  {\footnotesize
    \begin{tabular}{cccccccccc}
      \textbf{1st digit} $d$ & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9\\
      $p(D_1=d)$ & 0.301 & 0.176 & 0.125 & 0.097 & 0.079 & 0.067 & 0.058 & 0.051 & 0.046
    \end{tabular}
  }
  \begin{center}
    \includegraphics[width=.5\textwidth]{benford_law_bars.png}
  \end{center}
\end{frame}
%
\begin{frame}[label={sec:orgd7b02e3}]{Generalization to \(k\) digits}
  \begin{block}{Hill (1995)}
    \[
      p(D_1=d_1, D_2=d_2, ..., D_k=d_k) = \log_{10} \left\{1 + (d_1d_2...d_k)^{-1}\right\}
    \]
  \end{block}

  \begin{block}{Example:}
    Probability for observing the first and second digit to be \(32\) is:
    \[
      p(D_1=3, D_2=2) = \log_{10} \left\{1 + \frac{1}{32}\right\} \simeq 0.01334
    \]
  \end{block}
\end{frame}
\begin{frame}[label={sec:orgdd833f5}]{\ldots{} which yields the following 'Benford table'}
  \begin{figure}[htbp]
    \centering
    \includegraphics[width=.9\linewidth]{2023-02-02_11-57-24_screenshot.png}
    \caption{Reproduced from \cite{Diekmann2007}}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Benford's law is scale and base invariant}
  \begin{block}{Scale invariance}
    Rescaling (e.g. km$^2$ to mile$^2$) does not alter the 1st digit frequencies 
  \end{block}
  \begin{block}{Base invariance}
    Converting the number base (e.g. from decimal to binary) does not alter the
    1st digit frequencies 
  \end{block}
  \begin{block}{Theorem}
    Benford's law is the only scale and base invariant distribution\footfullcite{Hill1995}
  \end{block}
\end{frame}
%
\label{sec:org4c76816}
\begin{frame}[label={sec:org75d031d}]{S. Newcomb (1835 - 1909)}
% In 1881, US astronomer Simon Newcomb found that pages in a book of logarithms are more
% heavily worn on pages with numbers that start with 1 than on pages with higher numbers
% [Newcomb1881]. He
% concluded that numbers starting with 1 must be more abundant than other numbers and derived a mathematical expression for
% the probability to observe 1 as a leading digit.
  \begin{columns}
      \begin{column}{0.49\columnwidth}
        \begin{figure}[htbp]
          \centering
          \includegraphics[height=.7\textheight]{Simon_Newcomb_01.jpg}
          \caption{S. Newcomb (wikipedia)}
        \end{figure}
        \end{column}
        %
        \begin{column}{.49\columnwidth}
          \begin{itemize}
          \item Astronomer
          \item[1881]: Logarithm table pages for numbers starting with 1 more heavily
            worn than other digits
          \item Published first mathematical treatment of first-digit distribution\footfullcite{Newcomb1881}
          \end{itemize}
        \end{column}
      \end{columns}
    \end{frame}
  % 
\begin{frame}{F. Benford (1883 - 1948)}
  \begin{columns}
    \begin{column}{0.49\columnwidth}
      \begin{figure}[htbp]
        \centering
        \includegraphics[height=.7\textheight]{Frank_Benford.jpg}
        \caption{F. Benford (wikipedia)}
      \end{figure}
    \end{column}
    % 
    \begin{column}{.49\columnwidth}
      \begin{itemize}
      \item engineer/physicist
      \item rediscovered Newcomb's 1st digit distribution
      \item collected 20000 numbers
        from diverse sources such as "\ldots{} baseball, atomic weights, areas
        of rivers, numbering of articles in magazines" to support
        his "law", published in 1938\footfullcite{Benford1938}.
        \pause
        \item The lengths of his datasets do not follow his law...
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Explaning Benford's law (the handwaving way)\footfullcite{Fewster2009}}
\begin{itemize}
\item Benford's law holds for datasets spanning several orders of magnitude
\end{itemize}
    \begin{center}
      \includegraphics[width=.9\linewidth]{benford_lognormal_stripes}
    \end{center}
    \begin{itemize}
    \item digit 1 intervals $[0.1\ldots 0.2)$, $[1\ldots 2)$, $[10\ldots 20)$, \ldots  make up 30\% of a logarithmic scale
    \item[$\Rightarrow$] On average, 30\% of all numbers will have 1st digit 1.
    \end{itemize}
\end{frame}

\begin{frame}[label={sec:org62af625}]{Poisson distributions do not \textbf{not}
    follow Benford's law}
\begin{block}{}
\begin{center}
\includegraphics[width=.9\linewidth]{benford_poisson_l1.png}
\end{center}
\end{block}
\end{frame}

% \begin{frame}[label={sec:orga7a658c}]{Distribution testing}
% \begin{block}{Poisson}
% \begin{center}
% \includegraphics[width=.9\linewidth]{benford_poisson_l5.png}
% \end{center}
% \end{block}
% \end{frame}

% \begin{frame}[label={sec:org358cfb2}]{Distribution testing}
% \end{frame}
% \begin{frame}[label={sec:org99b61b5}]{Distribution testing}
% \begin{block}{Poisson}
% \begin{center}
% \includegraphics[width=.9\linewidth]{benford_poisson_l10.png}
% \end{center}
% \end{block}
% \end{frame}

% \begin{frame}[label={sec:org6bdd8bb}]{Distribution testing}
% \begin{block}{Uniform}
% \begin{center}
% \includegraphics[width=.9\linewidth]{benford_uniform_-1-1.png}
% \end{center}
% \end{block}
% \end{frame}

% \begin{frame}[label={sec:org32c707b}]{Distribution testing}
% \begin{block}{Log-Uniform}
% \begin{center}
% \includegraphics[width=.9\linewidth]{benford_loguniform_-1-1.png}
% \end{center}
% \end{block}



\begin{frame}{Explaning Benford's law (the rigourous way)\footfullcite{Smith1997}}
  \begin{itemize}
  \item Scale invariance $\Leftrightarrow$ Properties of Fourier Transform
  \end{itemize}
  \begin{center}
    \only<1>{%
    \includegraphics[width=.9\linewidth]{smith2002_benford_theorem.png}
  }
  \only<2>{%
    \includegraphics[width=.9\linewidth]{smith2002_fig34-6.png}
    }
  \end{center}
\end{frame}


\label{sec:org84d931f}
\begin{frame}[label={sec:orga07106f}]{Application to tax fraud detection\footfullcite{Geyer2004}}
\begin{figure}[htbp]
\centering
\includegraphics[width=.7\textwidth]{2023-02-02_14-27-58_screenshot.png}
\end{figure}
\end{frame}

% \begin{frame}[label={sec:orged74023}]{(In)famous examples}
% \begin{block}{E. Poehlmann}
% \begin{itemize}
% \item published 10 articles containing fabricated data to support his study of metabolic
% changes in women.
% \item first scientist to be arrested for scientific fraud
% \item Studies cost US \$ 2.9M
% \end{itemize}
% \end{block}
% \end{frame}

% \begin{frame}[label={sec:orgfbc0ad2}]{(In)famous examples}
% \begin{block}{D.P. Han}
% \begin{itemize}
% \item Tampered with HIV research data
% \item Fined 4.5 years in prison and US\$ 7.2M
% \end{itemize}
% \end{block}
% \end{frame}

% \begin{frame}[label={sec:org788a243}]{(In)famous examples}
% \begin{block}{W.S. Hwang}
% \begin{itemize}
% \item published two landmark papers in Science about human stem-cell resarch, claiming
% sucessful cloning of human stem cells
% \end{itemize}
% \end{block}
% \end{frame}

% \begin{frame}[label={sec:org13f03b0}]{(In)famous examples}
% \begin{block}{J.H. Schön}
% \begin{itemize}
% \item Became famous for experimental work on organic semiconductors
% \item "on track for nobel prize"
% \item competitors were able to show he used the same noise in many different figures
% (e.g. at different temperatures).
% \item Expert commitee report presents proof for 16 scientific misconducts (whole datasets
% being re-used in different experiments, experimental data produced from mathematical functions.
% \item Only Schön was found guilty, head of group and co-author was exonerated.
% \item Schön's Ph.D. revoked, case went all the way to the federar court where the University's decision was confirmed.
% \item 
% \end{itemize}
% \end{block}
% \end{frame}
\section{Application to omics data}
\label{sec:org8119d5d}
\begin{frame}[label={sec:orge49e28f}]{Detecting data fabrication in omics data\footfullcite{Bradshaw2021}}
\begin{block}{}
  \centering
  \includegraphics[height=.6\textheight]{Screenshot_2024-04-24_18-09-11}
\end{block}
\end{frame}
\begin{frame}
\begin{block}{Generation of (fake) test data from (real) Copy Number Variation data}
\begin{columns}
  \begin{column}{.49\textwidth}
    \centering
    \includegraphics[height=.7\textheight]{pone.0260395.s001.png}
  \end{column}
  \begin{column}{.49\textwidth}
    \centering
    \includegraphics[height=.7\textheight]{pone.0260395.s002.png}
  \end{column}
\end{columns}
\end{block}
\end{frame}
%
\begin{frame}[label={sec:orgf45976b}]{ML on leading digits outperforms ML on raw data\footfullcite{Bradshaw2021}}
\begin{columns}
\begin{column}{0.49\columnwidth}
  \begin{block}{Machine learning on leading digits}
    \centering
    \includegraphics[height=.5\textheight]{journal.pone.0260395.g003.PNG}
  \end{block}
\end{column}
%
\begin{column}{0.49\columnwidth}
  \begin{block}{Machine learning on count data}
    \centering
    \includegraphics[height=.5\textheight]{journal.pone.0260395.g002.PNG}
  \end{block}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:org43c0dff}]{Random Forest ML on digits provide >95\% accuracy even
    for datasets with as few as 20 observations}
\begin{figure}[htbp]
\centering
\includegraphics[width=\textwidth]{2023-02-02_14-35-20_screenshot.png}
\caption{Reproduced from Bradshaw (2021) under CC-BY 4.0 License}
\end{figure}
\end{frame}

\begin{frame}[label={sec:org23f43ca}]{Applicability to other omics data}
  % \begin{block}{}
  %   \centering 
  %   \includegraphics[width=.8\textwidth]{Screenshot_2023-02-07_22-48-54}\\
  %   {\tiny \fullcite{Bradshaw2021}}
  % \end{block}
\end{frame}

\label{sec:orgc79017a}

\begin{frame}[label={sec:org99d0e51}]{Benford analysis of MPB RNASeq Data (MPGC Projects 4293, 4994)}
\begin{block}{1st digit frequencies are perfectly Benford distributed}
\begin{center}
\includegraphics[height=.6\textheight]{2023-02-02_20-47-49_screenshot.png}
\end{center}
\end{block}
\end{frame}

\begin{frame}[label={sec:org0f3cf7a}]{Benford analysis of MPB RNASeq Data (MPGC Projects 4293, 4994)}
\begin{block}{Leading 2 digit frequencies are perfectly Benford distributed}
\begin{center}
  \includegraphics[height=.6\textheight]{2023-02-02_20-51-05_screenshot.png}
\end{center}
\end{block}
\end{frame}
\begin{frame}[label={sec:org5bd2a8e}]{Individual samples are Benford distributed}
\begin{center}
\includegraphics[width=\linewidth]{2023-02-06_14-50-05_screenshot.png}
\end{center}
\end{frame}
\begin{frame}[label={sec:org4db4f51}]{Generation of training and test datasets
    for Machine Learning (RandomForest)}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \begin{itemize}
      \item From 45 RNASeq samples (MPGC 4293):
      \item draw 20 training, 20 test samples
      \item Mix with 10 fake training, 10 fake test samples
        \begin{itemize}
        \item Randomization
        \item Random replacement (A)
        \item Resampling (B)
        \item Imputation (C, todo)
        \end{itemize}
      \end{itemize}
    \end{column}
    \begin{column}{.49\textwidth}
     \centering 
     \includegraphics[height=.6\textheight]{pone.0260395.s001.png}
    \end{column}
  \end{columns}
  {\tiny
  \fullcite{Bradshaw2021}
  }
\end{frame}
\begin{frame}{Preliminary results indicate perfect discrimination between fake
    and real data}
  \begin{block}{}
    \begin{tabular}[h]{ccccc}
      \hline
      \textbf{Method} & Randomization & Random replacement & Resampling \\
      \textbf{Accuracy} & 1.0 & 1.0 & 1.0 \\
      \hline
    \end{tabular}
  \end{block}
\end{frame}
%\section{Conclusions}
\begin{frame}{Conclusions I}
\begin{block}{}
  \begin{itemize}
  \item Fabrication, Falsification, and Plagiarism are prevalent problems in
    science, with potentially dramatic consequences for cheaters and their colleagues
    \item Countermeasures include but are not limited to detection of
      FFP
    \item Benford's law (BL) predicts the frequency of first digits in large-scale distributions
    \item BL has successfully been applied to tax fraud detection
    \item Recent research suggests BL is applicable to detect fabricated omics data
    \item Our results for RNASeq data strongly support this finding
    \item Other methods include variance analysis and distribution testing
  \end{itemize}
\end{block}
\end{frame}
%
\begin{frame}[label={sec:orge86b169}]{Conclusions II}
  \begin{itemize}
  \item Statistical methods can only ever indicate towards or against
    fraud, but never proof.
  \item Indications for fraud must always be treated with uttermost care and
    discretion (see above).
  \item Not touched upon: Image data fraud, Deep fakes (incl. ChatGPT), \ldots
  \end{itemize}
  \end{frame}
  \begin{frame}{Conclusions III}
  \begin{block}{}
    Having to detect scientific fraud essentially means, the damage has been
    done. How can we (as individuals (staff, supervisors, students, post--docs))
    and as collectives (groups, departments, institutes, society) create a
    working atmosphere that minimizes the perceived or actual factors that
    motivate fraud?
    \begin{itemize}
    \item Pressure to publish \& academic performance indicators
    \item Negative results ``not publishable''
    \item Need to present complete stories (even in lab seminars)
    \end{itemize}
  \end{block}
\end{frame}
%
\begin{frame}{Data and Computing Consultancy}
  \centering
  The data and computing consultancy is an open forum to share and discuss data
  and analysis.\\[2em]

  Every Thursday 15-17h in the Villa Seminar Room!\\[2em]


  All Welcome!
\end{frame}


\begin{frame}[label={sec:org4e623c7}]{The End}
\alert{Thank you very much!}
\end{frame}

\section*{Additional slides}
\begin{frame}[label={sec:org25dfd7b}]{Even fabricated data exhibit Benford distribution of first digits\footfullcite{Diekmann2007}}
  \begin{block}{}
    \begin{itemize}
    \item Benford tests should rather focus on later digits
    \end{itemize}
    \begin{center}
      \includegraphics[width=.7\linewidth]{2023-01-27_13-50-02_screenshot.png}
    \end{center}
  \end{block}
\end{frame}
%
\begin{frame}[label={sec:org1719863}]{Checking the last digit in hand-recorded data\footfullcite{Lawson2019}}
  \begin{block}{Human's tend to round off to 0 or 5 in the last digit}
    \begin{figure}[htbp]
      \centering
      \includegraphics[width=.7\textwidth]{2023-02-06_11-15-30_screenshot.png}
    \end{figure}
  \end{block}
\end{frame}
%
\begin{frame}[label={sec:org65b7193}]{Distribution testing}
  \begin{block}{}
    If the underlying statistical distribution can be assumed, the likelihood
of the presented data for belonging to that distribution can help identifying odd records.
\end{block}
\end{frame}
%
\begin{frame}[label={sec:orgc83da5a}]{Detection of changes in mean and variance in skin-prick allergy test data\footfullcite{Lawson2019}}
  \begin{block}{Positive control}
    \begin{center}
      \includegraphics[width=.7\linewidth]{2023-02-07_09-01-01_screenshot.png}
    \end{center}
  \end{block}
\end{frame}
%
\begin{frame}[label={sec:orgf455290}]{Detection of changes in mean and variance in skin-prick allergy test data\footfullcite{Lawson2019}}
  \begin{block}{Negative control}
    \begin{center}
      \includegraphics[width=.7\linewidth]{2023-02-07_09-02-07_screenshot.png}
    \end{center}
  \end{block}
\end{frame}
%
\end{document}
