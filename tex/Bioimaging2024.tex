\documentclass[bigger, aspectratio=169]{beamer}

\usepackage{multimedia}
\usepackage[bottom]{footmisc}
\usepackage[capitalize]{cleveref}
\usepackage[normalem]{ulem}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[style=authoryear, maxcitenames=1]{biblatex}
\usepackage{algorithm2e}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amsthm}
\usepackage{biblatex}
\usepackage{calc}
\usepackage{capt-of}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{glossaries}
\usepackage{longtable}
\usepackage{marvosym}
\usepackage{multicol}
\usepackage{pslatex}
\usepackage{rotating}
\usepackage{subcaption}
\usepackage{wrapfig}

\usepackage{hyperref}
\usepackage{hyperxmp}


\addbibresource{jabref.bib}

% Acronyms and typesavers
\newcommand{\pflu}{\textit{Pseudomonas fluorescens} }
\newacronym{tl}{TL}{Transfer Learning}
\newacronym{cnn}{CNN}{Convolutional Neural Network}
\newacronym{dnn}{DNN}{Deep Neural Network}
\newacronym{ml}{ML}{Machine Learning}


\makeglossaries


\addbibresource{jabref.bib}
\crefname{section}{Sec.}{Secs.}
\Crefname{section}{Section}{Sections}
\Crefname{table}{Table}{Tables}
\crefname{table}{Tab.}{Tabs.}
\crefname{equation}{Eq.}{Eqs}
\Crefname{equation}{Equation}{Equations}
\crefname{figure}{Fig.}{Figs}
\Crefname{figure}{Figure}{Figures}
\mode<beamer>{\usetheme{Madrid}}
\usetheme{default}
\author[C. Fortmann-Grote]{Beate Gericke, Finn Degner, Tom Hüttmann, Sören Werth, \uline{Carsten Fortmann-Grote}}
\date{Bioimaging 2024\\Feb.  22  2024}
\title[Performance Review]{Performance Review of Retraining and Transfer Learning of DeLTA2 for Image Segmentation for Pseudomonas Fluorescens SBW25}
\subtitle{Paper BIOIMAGING24-RP-43}
\hypersetup{
  pdfauthor={Carsten Fortmann-Grote},
  pdftitle={Performance Review of Retraining and Transfer Learning of DeLTA2 for Image Segmentation for Pseudomonas Fluorescens SBW25},
  pdfkeywords={Deep learning, UNet, Delta2, Pseudomonas fluorescens SBW25},
  pdfsubject={BIOIMAGING24-RP-43},
  pdfdate={D:20240222090000Z},
  pdflang={English}
}

\renewcommand{\footnotesize}{\scriptsize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                        DOCUMENT STARTS HERE                             %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\begin{document}
\begin{frame}
  \maketitle
\end{frame}
% 
\section{\uppercase{Introduction}}
\subsection{Deep Learning tackles big bioimage data}
\begin{frame}
  \frametitle{\subsecname}
  \begin{block}{4 Bioimage repositories total volume (April 2023)}
    \only<1->{%
    \begin{center}
      \includegraphics[width=0.99\linewidth]{databases_in_tb_joshmoore_20230418.png}\\
      \tiny{Credits: J. Moore}
    \end{center}
  }
  \only<2->{%
    \begin{description}
    \item[Hardware] High throughput thanks to Graphics Processing Units (GPUs)
    \item[Software] High performance deep learning libraries
    \item[(Re)usability] Well designed and documented APIs for reusing code in
      many applications
    \item[Generizability] Once trained models work for many diverse datasets
    \end{description}
  }
\end{block}
\end{frame}
%
\begin{frame}
\frametitle{Research background}
\begin{columns}[T]
  \begin{column}{.48\textwidth}
    \begin{block}{From timelapse microscopy...}
      \begin{center}
        \includegraphics[height=.7\textheight, clip]{dataset2159_timelapse.pdf}\\[-1ex]
        \tiny{Pseudomonas fluorescens SBW25 phase contrast microscopy time series}
      \end{center}
    \end{block}
  \end{column}
  \begin{column}{.48\textwidth}
    \begin{block}{... to fitness assays}
      \begin{center}
        \includegraphics[width=\textwidth]{sketch_growthcurves.png}\\
        \tiny{Schematic growth curves representing differential fitness between mutant and wildtype}
      \end{center}
    \end{block}
  \end{column}
\end{columns}
\end{frame}


\subsection{Research questions}
\begin{frame}
  \frametitle{\subsecname}
  \begin{itemize}
  \item Do off-the-shelf deep learning models yield accurate (enough) cell segmentations for phase contrast images?
  \item What's the best way to systematically refine a model for our data?
    \begin{itemize}
    \item Retraining
    \item Transfer learning
    \end{itemize}
  \end{itemize}
\end{frame}
\subsection{Application to Pseudomonas fluorescens SBW25}
\begin{frame}
  Large amounts of microscopy data started to accumulate in 2020, calling for
  the implementation of a standard segmentation model. In 2021, Beate started
  her Master thesis on this topic. 

\end{frame}
\begin{frame}
 \frametitle{Deep learning implementations for cell segmentation}
 \begin{columns}[T]
   \begin{column}{.40\linewidth}
     \begin{itemize}
     \item<1-> MiSiC \parencite{Panigrahi2021}
     \item<2-> \textbf{DeLTA2} \parencite{OConnor2022}
     \item<3-> Cellpose/Omnipose \parencite{Cutler2022}
     \end{itemize}
   \end{column}
   \begin{column}{.55\linewidth}
     \begin{center}
       \only<1>{%
         \includegraphics[width=.7\columnwidth]{Panigrahi2021_Fig1.png}%
       }%
       \only<2>{%
         \includegraphics[width=.8\columnwidth]{delta2_model_journal.pcbi.1009797.g001.PNG}%
       }%
       \only<3>{%
         \includegraphics[width=.8\columnwidth]{Cutler2022_Fig1.png}%
       }%
     \end{center}
   \end{column}
 \end{columns}
\end{frame}
%

\subsection{Training and test datasets}
\begin{frame}
 \frametitle{\subsecname} 
 \begin{columns}[T]
   \begin{column}{.45\textwidth}
     \begin{block}{}
       \begin{center}
         \includegraphics[width=.8\columnwidth]{2159_s01_T00-0}\\
         {\tiny\href{http://ome.evolbio.mpg.de/webclient/img_detail/2404230/?dataset=2159}{omero
           dataset:2159}}
       \end{center}
     \end{block}
   \end{column}
   \begin{column}{.45\textwidth}
     \begin{itemize}
      \item MPB24521
      \item phase constrast
      \item 100x magnification
      \item 34 time series
     \end{itemize}
   \end{column}
 \end{columns}
\end{frame}
%
\begin{frame}
 \frametitle{\subsecname} 
 \begin{columns}[T]
   \begin{column}{.45\linewidth}
     \begin{block}{}
       \begin{center}
         \includegraphics[width=.8\columnwidth]{2159_s01_T15_img_raw_mask.jpg}
       \end{center}
     \end{block}
   \end{column}
   \begin{column}{.45\linewidth}
     \begin{itemize}
     \item Manually mark regions on graphical tablet
      \item Adjacent, overlapping cells artificially separated (1px boundary)
      \item 412 labelled images
      \item Training--validation--test split = 308-88-16
     \end{itemize}
   \end{column}
 \end{columns}
\end{frame}
%
\subsection{Baseline model: DeLTA-2.0 U-Net}
\begin{frame}{\subsecname}
    \begin{center}
      \includegraphics[width=.6\linewidth]{delta2_model_journal.pcbi.1009797.g001.PNG}\\
      {\tiny\parencite{OConnor2022}}
  \end{center}
\end{frame}
%
\begin{frame}
  \frametitle{\subsecname}
  \begin{block}{The baseline model strongly oversegments our training data}
  \begin{figure}[t]
      \begin{subfigure}{0.32\linewidth}
        \begin{center}
          \includegraphics[width=.6\linewidth, viewport=0 50 253 505, clip]{2159_s01_T15_roi_raw.flip.png}
          \caption{Raw data}
          \label{fig:raw_roi}
        \end{center}
        \end{subfigure}
        \begin{subfigure}{.32\linewidth}
          \begin{center}
            \includegraphics[width=.6\linewidth, viewport=0 50 253 505, clip]{2159_s01_T15_roi_mask.flip.png}
            \caption{Manual ground truth}
            \label{fig:mask_roi}
          \end{center}
        \end{subfigure}
        \begin{subfigure}{.32\linewidth}
          \begin{center}
            \includegraphics[height=.6\linewidth, angle=90]{delta_pretrained_model_prediction_overlay.png}
            \caption{TP, \textbf{TN}, \color{red}{FN}, \color{green}{FP}}
            \label{fig:delta-prediction}
      \end{center}
      \end{subfigure}
      \label{fig:pipeline-test}
  \end{figure}
\end{block}
\end{frame}


\section{\uppercase{Experiment Outline}}
\label{sec:experiment_outline}
\mode<article>{%
We consider how the DeLTA-2.0 model performs on phase contrast images of \textit{Pseudomonas fluorescens} SBW25 with and without retraining. The steps of our experiment are explained the following sections:
}

\begin{frame}
  \begin{enumerate}
  \item Data collection
  \item Ground Truth Masks
    \begin{itemize}
    \item manual labeling of all training images\\
      $\to$ \textbf{\color{blue}{complete dataset}}
    \item semi--automateg labeling of low--complexity images (only well
      separated cells)\\
      $\to$ \textbf{\color{orange}{partial dataset}}
    \end{itemize}
  \item Training Process
    \begin{itemize}
    \item \textbf{retraining} with \textbf{\color{blue}{complete dataset}}
    \item \textbf{retraining} with \textbf{\color{orange}{partial dataset}}
    \item \textbf{transfer--learning} with \textbf{\color{blue}{complete dataset}}
    \item \textbf{transfer--learning} with \textbf{\color{orange}{partial dataset}}
    \end{itemize}
  \item Evaluation
    \begin{itemize}
    \item Evaluate trained models on test split (only complete dataset)
      \item Quantify Balanced Accuracy (BA) and Intersection--over-Union (IoU)
    \end{itemize}
  \end{enumerate}
\end{frame}
\subsection{Transfer Learning\label{sec:results-transfer}}
\mode<article>{%
We evaluate various models based on the original DeLTA-2.0 baseline with varying
numbers of retrained layers (transfer learning): 36 layers (corresponding to
complete retraining), 27, 18, 9, and 2 layers.
Table~\ref{tab:metrics-tl-complete-retraining} lists the results for balanced
accuracy and for IoU averaged over all images in the test data. The confidence
interval
is taken as the twofold standard deviation of the evaluations of three
independently trained model replicates.
}
% 
\begin{frame}
  \frametitle{\subsecname} 
  \begin{columns}[T]
    \begin{column}{.45\linewidth}
      \begin{block}{Transfer learning with frozen layers}
        \begin{center}
          \includegraphics[width=.8\columnwidth]{chollet_transfer-learning.png}\\
          {\tiny Credits: \parencite{Chollet2018}}
        \end{center}
      \end{block}
    \end{column}
    \hfill
    \begin{column}{.45\linewidth}
      \begin{itemize}
      \item Full retraining is expensive in time and memory
      \item CNNs are sequence of filters: Early layers sensitive to local,
        generic features (edges)
        later layers sensitive to abstract features, defining the class of object
      \item[$\to$] TL: Train only ``specific'' filters, freeze ``generic'' filters
      \end{itemize}
      
    \end{column}
  \end{columns}
\end{frame}
% 
\frame{%
  \frametitle{\subsecname\hfill\parencite{Chollet2018}} 
  \begin{columns}[T]
    \begin{column}{.45\linewidth}
      \begin{block}{Input image}
        \begin{center}
          \includegraphics[width=.8\columnwidth,angle=0,clip]{testcat.jpeg}
        \end{center}
      \end{block}
    \end{column}
    \hfill%
    \begin{column}{.45\linewidth}
      \begin{block}{Filter activations}
        \begin{center}
          \includegraphics[width=.6\columnwidth,angle=0,clip]{testcat_allchannels.jpeg}%
        \end{center}
      \end{block}
    \end{column}
  \end{columns}
}

\subsection{Metrics}\label{sec:metrics}
\begin{frame}
  \frametitle{\subsecname}
  \begin{xalignat}{3}
    T\!P &= \left| A\cap B  \right|       & F\!P &= \left|A \cap\lnot B  \right|\\
    F\!N &= \left| \lnot A\cap B \right|  & T\!N &= \left|\lnot A\cap \lnot B \right|\ ~,
  \end{xalignat}
  with $A=$ model prediction and $B=$ ground truth
  % 
  \begin{align}
    \text{Balanced Accuracy} = \frac{\text{Recall} + \text{Specificity}}{2}
    \label{eq:balanced_accuracy}
  \end{align}
  with:
  \begin{xalignat}{3} \label{eq:recall} \text{Recall} &= \frac{T\!P}{T\!P + F\!N
                                                        } & \text{Specificity} &= \frac{T\!N}{T\!N + F\!P}
  \end{xalignat}

  \begin{equation}
    \text{IoU} = \frac{\mid A \cap B \mid}{\mid A \cup B \mid} 
    = \frac{T\!P}{T\!P + F\!N + F\!P} 
    \label{eq:iou}
  \end{equation}
\end{frame}


\section{\uppercase{Dataset creation}}
\label{sec:dataset_creation}

\mode<article>{%
\subsection{Microscopy}
DeLTA-2.0 only uses the phase contrast channel of the image, the other channels are ignored \parencite{OConnor.2022}.

Microscopy was performed at MPI Evolutionary Biology with a Axio Imager Z2 (Zeiss) microscope
at 100x magnification. Multiple timeseries consisting of 10-20 images with different frame rates were generated.
All image data is available from \parencite{Fortmann-Grote2023_edmond}.

\subsection{Mask creation}
The dataset was created from time-lapse microscopy images, hence it contains images with different amounts of cells. We tried different methods for automated mask generation. To automatically generate masks for Images with only a few cells tools like MicrobeJ \parencite{Ducret.2016} achieved sufficient results. But for images containing cell clusters or a larger number of touching cells the results were not reliable, hence the mask generation required manual work. 
To generate the masks, the timelines were split into single images. The brightness and contrast of the images needed to be adjusted to make the differentiation between background and cells visible for the human eye. These edited images were only used for the mask creation, the training was performed on the images with original brightness and contrast. 
The masks were created using Adobe Photoshop (version 23.5.1) \parencite{AdobeInc..} and Affinity Photo (version 1.10.6) \parencite{SerifLtd..2022} using a tablet and pen resp. a graphic tablet. The masks were created making the borders as small as possible, while still separating the cells. While the masks for images with only a few cells were easy to create, creating masks for images with large clusters was time consuming and the separation of the cells became more challenging. For the retraining a dataset with 412 images from 34 time series was created, this dataset is referred to as the \textit{complete dataset}. 

To create the \textit{partial dataset}, images at the end of the time series were dropped from the complete dataset ($t> 6$), which means that the images used had smaller cell clusters, hence were less complex. The reduction resulted in a dataset containing 226 images from 34 time series. This approach not only allows us to observe if more complex data yields better performance in learning, but also reduces resources needed for data creation. We used the  MicrobeJ plugin (version MicrobeJ 5.13n (8) - beta) for images early in the timeline (Algorithm for threshold: modified IsoData algorithm; Offset threshold: 180; Thresholding: Stack Histogram; Resampling Method: Bicubic (p = 0.5)).

\subsection{Training-Validation-Test-Split}\label{sec:training_test_split}
To evaluate the performance of a model a test dataset which the network has never seen before should be used so the network is not biased toward the test data \parencite{Glassner.2021}.
For our training runs with the full MPB dataset 16 images from one time series were set aside in test datasets, 308 images from 26 time series were used as training set and 88 images from 7 time series were used as validation set.
Having images from one time series in the test dataset gives us the opportunity to observe if the performance of the prediction decreases with growing cell clusters.
For the training with the partial MPB dataset the test dataset stays untouched, for the training set 88 images were used and 49 images were used for the validation set.
For the validation set full time series were used to prevent leaky validation.
}

\section{\uppercase{Training process}}
\mode<article>{%
DeLTA-2.0 uses weight maps to emphasize important parts of an image. These were regenerated for the DeLTA-2.0 dataset, to have a consistent workflow for all trained models. For this the DeLTA-2.0 function for creating the weight maps was used and the weight maps saved for the training process. For the training process a script provided by the development team of DeLTA-2.0 was adapted, training for 600 epochs with 300 steps each and early stopping with a patience of 50 epochs with the Adam optimizer with a learning rate of 0.0001. The loss function is the pixel wise weighted binary cross entropy implemented by DeLTA-2.0.

The base for the \gls{tl}is the pre-trained model for the DeLTA-2.0 segmentation network which is made from 36 layers. The training process was done in 10 epochs with 300 steps which was applied on 5, 9, 18 and 36 layers using the weights of the DeLTA-2.0 pre-trained model as the initializing weights, resulting in 4 \gls{tl} models.
The training process was defined with 10 epochs with 300 steps, hence the training time was significantly partial which allowed to perform the different experiments in a reasonable amount of time. As in the retraining the Adam optimizer with a learning rate of 0.0001 is used and as a loss function the weighted pixel wise binary cross entropy implemented by DeLTA-2.0 is used.
}
\mode<article>{%
Balanced accuracy was suggested as a suitable metric for skewed (imbalanced) datasets;
the IoU can be shown to be the strictest evaluation metric for reasonably well
performing classification tasks (see \cref{sec:appendix_iou_vs_ba}).

We investigated how different retraining
approaches affect the segmentation performance as measured by the balanced
accuracy metric and the intersection--over--union (IoU) metric
(see Results section). In general the IoU is considered a more reliable metric for image segmentation
than accuracy \parencite{Minaee2020}. Moreover, in imbalanced datasets
(foreground/background ratio large or small), accuracy is a
misleading metric and it is recommended to use the balanced accuracy instead
\parencite{Garcia.2009}.
}
\section{\uppercase{Results}} \label{sec:results}

\mode<article>{%
\subsection{Retraining\label{sec:results-retraining}}
We evaluated our retrained models and compared against the DeLTA-2.0 baseline.

The model resulting from a complete retraining with the complete MPB dataset
achieves a mean Balanced Accuracy of 0.90 and a mean IoU of 0.77 which is a
significant performance gain compared to a mean Balanced Accuracy of 0.85 and
a mean IoU of 0.65 achieved by the DeLTA-2.0 pre-trained model.
}

\section{Results}
\subsection{Retraining the complete DeLTA2 model}
\begin{frame}{\secname: \subsecname}
  \begin{figure}
    \centering
    \begin{subfigure}{0.32\linewidth}
      \includegraphics[width=0.99\linewidth]{DeLTA_2.0_model.jpg}
      \caption{DeLTA-2.0 model evaluated on MPB test dataset.}
      \label{fig:short-a}
      $BA=0.85, IoU=0.65$
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.32\linewidth}
      \includegraphics[width=0.99\linewidth]{full_retrain_full_dataset.jpg}
      \caption{MPB model completely retrained with complete MPB dataset.
     }
     \label{fig:short-b}$BA=0.9, IoU=0.77$
   \end{subfigure}
    \hfill
    \begin{subfigure}{0.32\linewidth}
      \includegraphics[width=0.99\linewidth]{full_retrain_reduced_dataset.jpg}
      \caption{MPB model completely retrained with partial MPB dataset.}
      \label{fig:short-c}
      $BA=0.9, IoU=0.77$
    \end{subfigure}
    \label{fig:retraining-overlay}
  \end{figure}
\end{frame}

\mode<article>{%
In \cref{fig:retraining-overlay} we show a color coded comparison between model
evaluations on test data and ground truth. Mask pixels correctly identified as
belonging to a cell are highlighted in white (true positives), false positives
are colored in red, false negatives are colored green and true negatives in
black. 
While the baseline model DeLTA-2.0 (\cref{fig:short-a}) yields a high false--negative rate (indicated by green areas), retraining on either the complete (\cref{fig:short-b}) or the partial (\cref{fig:short-c}) MPB dataset significantly reduces the false--negative rate. On the other hand, the false--positive rate slightly increases compared to the baseline (indicated by red areas) but a net improvement in overall balanced accuracy and IoU remains.
}


\subsection{Transfer Learning}
\subsubsection{Protocol}
\begin{frame}
  \frametitle{\subsecname\ \subsubsecname}
  \begin{center}
    \begin{itemize}
      \item Transfer learning with 2\ldots36 retrained layers
      \item 3 replicate models
      \item[$\to$] 105 models
    \item Evaluate performance metrics for each model
    \item Group results by timepoint
    \item Mean, standard deviation over replicates
    \end{itemize}
  \end{center}
\end{frame}
\subsubsection{Complete Dataset}
\begin{frame}{\secname: \subsecname\ -- \subsubsecname}
  \begin{block}{Retraining top 25\% layers sufficient to achieve full retraining performance}
    \begin{center}
      \includegraphics[width=0.8\linewidth]{sbw25_TL_metrics_vs_Nlayers_2-9-18-27-36_with_means.pdf}
    \end{center}
  \end{block}
\end{frame}
%
\begin{frame}{\secname: \subsecname\ -- \subsubsecname}
  \begin{block}{Average over timeseries}
    \begin{center}
      \begin{tabular}{|c|c|c|}
        \hline
        & Mean BA & Mean IoU\\
        \hline
        DeLTA-2.0 model & $0.85\pm 0.04$ & $0.65\pm 0.04$ \\
        \hline
        36 layers model & $0.90\pm 0.03$ & $0.77\pm 0.05$ \\
        \hline
        27 layers model & $0.90\pm 0.02$ & $0.78\pm 0.04$\\
        \hline
        18 layers model & $0.91\pm 0.03$ & $0.79\pm 0.04$ \\
        \hline
        9 layers model & $0.89\pm0.04$  & $0.76\pm 0.06 $\\
        \hline
        2 layers model & $0.88\pm 0.04$ & $0.73\pm 0.06 $\\
        \hline
      \end{tabular}
    \end{center}
  \end{block}
\end{frame}

 
\mode<article>{%

\Cref{tab:metrics-tl-complete-retraining} tabulates the averaged balanced accuracy and IoU values obtained from the various \gls{tl} models with varying numbers of retrained layers. The confidence intervals are taken as the twofold standard deviation over three independently trained models and all timepoints. All \gls{tl} models improve the segmentation performance compared to the baseline model DeLTA-2.0.
The lowest accuracy gain is found when only retraining the last two layers of
the 36 layer U-Net, while the remaining models with 9, 18, 27, and 36 retrained
layers give nearly identical results. We find the same pattern when analyzing
the results for individual timesteps in our data as shown in \cref{fig:tl-a}.
The measured performance metrics values vary over the entire timeseries, with
the tendency of decreasing performance towards later timepoints where cells are
more abundant and start to accumulate and to overlap, making accurate
segmentation more difficult.

In the creation of our training dataset we assumed that for a performance gain,
especially for images containing cell clusters, we need a training dataset of a
sufficient size, containing equivalently complex training data. To assess this hypothesis,
we evaluated the performance of \gls{tl} models trained on the partial training
datasets, containing only well separated cells and no accumulations or closely
neighboring cells. \Gls{tl} was again executed with 2, 9, 18, 27, and 36 neural
network layers.
}
% 
\subsubsection{Partial Dataset}
    \begin{frame}{\secname: \subsecname\ -- \subsubsecname}
      \begin{block}{All models $\geq 9$ layers perform equally well}
    \begin{center}
      \includegraphics[width=0.8\linewidth]{sbw25_TL_metrics_vs_Nlayers_2-9-18-27-36_small_with_means.pdf}
    \end{center}
  \end{block}
\end{frame}
% 
\begin{frame}{\secname: \subsecname\ -- \subsubsecname}
  \begin{block}{Average over timeseries}
    \begin{center}
      \begin{tabular}{|c|c|c|}
        \hline
        & Mean BA & Mean IoU\\
        \hline
        DeLTA-2.0 model & 0.85 & 0.65 \\
        \hline
        36 layers model & $0.90\pm 0.04$ & $0.77\pm 0.06$\\
        \hline
        27 layers model & $0.90\pm 0.04$ & $0.78\pm 0.05$\\
        \hline
        18 layers model & $0.90\pm 0.04$ & $0.77\pm 0.06$ \\
        \hline
        9 layers model & $0.90\pm 0.04$ & $0.76\pm 0.06$ \\
        \hline
        2 layers model & $0.86\pm 0.04$ & $0.71\pm 0.07$ \\
        \hline
      \end{tabular}
    \end{center}
  \end{block}
\end{frame}
% 
\mode<article>{%
Table \cref{tab:metrics-tl-reduced-retraining} lists the mean values for balanced accuracy and IoU over the timeseries for the
partial training dataset.  We observe the same trend as in the case of the complete dataset: Training only a 9 layers yields the same performance
as with 18, 27, or 36 layers. Within the 2 sigma confidence interval, even the 2
layer case yields the same performance as the runs with more layers.
Both balanced accuracy and IoU values coincide with the respective values for the complete dataset evaluation (\cref{tab:metrics-tl-complete-retraining}). 

We compare our transfer learning results for the partial and for the complete training dataset in more detail in \cref{fig:sbw25_unet_10layer_small_vs_complete}. Within error bars, transfer learning with 10 retrained layers yields the same validation metrics in both cases at each timepoint in the series.
}

\subsection{Complete vs. partial dataset trained models}
\begin{frame}{\secname: \subsecname}
  \begin{block}{BA and IoU for 10 layer TL model trained on complete vs. partial
    datasets}
    \begin{center}
      \includegraphics[width=.8\linewidth]{sbw25_TL_metrics_vs_10layers_small_vs_complete.pdf}
    \end{center}
  \end{block}
\end{frame}
\mode<article>{%
Keeping in mind that the partial dataset contains only images with a few, well separated cells,
these results show that the performance is not correlated with the complexity of the dataset the model was trained on.

Evaluating the completely retrained models on both the complete and on the partial dataset yields the same result:
\Cref{fig:short-b} and \cref{fig:short-c} visualize the segmentation from the completely retrained model trained with the complete dataset
and the completely retrained model trained with the partial dataset. The generated masks from both models are indistinguishable by eye.
}

\section{Towards unsupervised segmentation}
\subsection{Conclusions}
\begin{frame}
  \scriptsize
  \frametitle{\subsecname}
  \begin{columns}[T]
    \begin{column}{.39\linewidth}
      \includegraphics[width=.8\columnwidth]{sbw25_TL_metrics_vs_10layers_small_vs_complete.pdf}
    \end{column}
    \begin{column}{.6\linewidth}
      \begin{itemize}
      \item Results suggest that segmentation models trained on sparse datasets
        (=few, well separated cells) have a high degree of generizability 
      \item Sparse images are well suited for classical (no
        learning) segmentation, e.g. thresholding. 
        \pause
      \item Proposed workflow:
        \begin{itemize}
          \scriptsize
        \item Select sparse images where cells are well defined and separated
          
        \item Create masks via thresholding 
        \item Train network with labeled, sparse dataset 
        \item Evaluate on full dataset containing adjacent, overlapping cells
        \end{itemize}
       \pause 
      \item[$\to$] Unsupervised (semi--automated) segmentation
      \item[$\to$] Manual annotation of large amounts of images
        becomes obsolete!
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}
\begin{frame}
  \frametitle{First \only<2>{failed} attempt at learning with thresholded training data}
  \begin{columns}[T]
    \begin{column}{.45\linewidth}
      \begin{center}
        \includegraphics[width=\columnwidth]{accuracy_vs_epoch_unsupervised.png}
      \end{center}
    \end{column}
    \begin{column}{.45\linewidth}
      \begin{center}
        \includegraphics[width=\columnwidth]{metrics_unsupervised.pdf}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}
\section{Outlook}
\begin{frame}
  \frametitle{\secname}
  \begin{center}
    \begin{itemize}
    \item Explore classical segmentation techniques to automate training data
    \item Transfer learning for Omnipose
    \item Fluorescence microscopy
    \end{itemize}
  \end{center}
\end{frame}

\section{Discussion}\label{sec:discussion}
\mode<article>{%
As expected, we observe a significant improvement in IoU and Balanced Accuracy
compared to the original DeLTA-2.0 model when retraining with our MPB training
dataset at the cost of reduced generizability. In view of our initial goal to
train a dedicated segmentation model for \textit{Pseudomonas fluorescens} SBW25,
our study can therefore be seen as a success. 

Since the MPB dataset is quite homogeneous, we did not expect it to perform well on
the more heterogeneous DeLTA-2.0 dataset containing images created under various
conditions and for another model organism.

We also confirmed that transfer learning improves the performance of our segmentation
model. Transfer learning can be especially useful if not much data for a
retraining is available or if compute time for retraining is limited.
The performance gain with \gls{tl} can be achieved with less complex datasets than in the case of complete retraining.
Since these less complex training data could also be accurately segmented with traditional
techniques based on feature extraction (e.g. intensity threshholding), whereas the more complex data with
accumulations of overlapping cells in many images must be labeled manually, our results indicate a possible route towards
unsupervised segmentation of bacterial cells. Such a semi--automated workflow
would consist of a) manual curation of training data (selection of low complexity images), b) segmentation by intensity threshholding to generate the
training labels, c) supervised training and evaluation of a deep neural network using the training labels, and finally c) application of the model
on the data to be analysed.
}
\mode<article>{
\section*{\uppercase{Acknowledgements}}

\appendix
\section{\uppercase{Intersection--over--Union vs. Balanced Accuracy}}
\label{sec:appendix_iou_vs_ba}

In the following we derive the inequality $\text{IoU} \leq \text{Balanced
Accuracy}$. To our knowledge such a derivation is missing in the literature.

According to \cref{eq:balanced_accuracy} and \cref{eq:iou}, the ratio of both metrics is: 
\begin{multline}
  \label{eq:ba_over_iou}
  \frac{\text{Balanced Accuracy}}{\text{IoU}}  =\\
  \frac{1}{2}\frac{\frac{TP}{TP + FN} + \frac{TN}{TN + FP}}{TP}\left( TP + FP + FN \right) \\
  = \frac{1}{2}\left[ \frac{TP + FP + FN}{TP + FN} + \frac{TN}{TP}\frac{TP + FP + FN}{TN + FP} \right] \\
                                                = \frac{1}{2}\left[ 1 + \underbrace{\frac{FP}{TP + FN}}_{I} + \underbrace{\frac{1+\frac{TP + FN}{TP}}{1+\frac{FP}{TN}}}_{II} \right] \\
\end{multline}

Assuming that the segmentation performs ``reasonably
well'', specifically $FN, FP \ll TN, TP$, we obtain
(\ref{eq:ba_over_iou}),
\[0 \leq I < 1~.\]
For the same reason
\[\frac{TP+FN}{TP} \geq 1\]
and \[1 \leq 1 + \frac{FP}{TN} < 2\]
and finally
\[
1  \leq II < 2~. 
\]

We can now write
\begin{equation}
  \frac{\text{Balanced Accuracy}}{\text{IoU}} = \frac{1}{2}\left[ 1 + \epsilon + 1 + \delta \right] \geq 1~,
\end{equation}
since $\epsilon<1$ and $\delta<1$.

In conclusion, it is fair to assume that for reasonably well performing
segmentation, balanced accuracy is always larger or equal to IoU. In other words, IoU is the stricter metric.
}
\end{document}
