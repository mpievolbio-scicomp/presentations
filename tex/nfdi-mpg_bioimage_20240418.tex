% Created 2024-04-22 Mon 12:20
% Intended LaTeX compiler: pdflatex
\documentclass[presentation]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{hyperxmp}
\mode<beamer>{\usetheme{Madrid}}
\usepackage{biblatex}
\usepackage{tikz}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{positioning}
\usetikzlibrary{calc}
\usetikzlibrary{arrows}
\usetheme{default}
\author{Carsten Fortmann-Grote\thanks{carsten.fortmann-grote@evolbio.mpg.de}}
\date{April 18 2024}
\title{NFDI4BIOIMAGE}
\titlegraphic{%
 \includegraphics[height=0.3\textheight]{Luftbild.jpg}\\
 \tiny This presentation is released to the public domain under Creative Commons Attribution License CC-BY-4.0 International.%
}
\logo{%
 \includegraphics[width=0.05\textwidth]{CC-BY-4.0_88x31.png}%
 \hspace{.02\textwidth}%
 \includegraphics[width=0.05\textwidth]{nfdi4bi_128x128.png}%
 \hspace{.02\textwidth}%
 \includegraphics[width=0.05\textwidth]{mpg_logo_FGgreen_BGalpha}
}
\hypersetup{
 pdfauthor={Carsten Fortmann-Grote},
 pdftitle={NFDI4BIOIMAGE},
 pdfkeywords={NFDI, Bio Imaging, Linked Data, Controlled Vocabulary, Knowledge Base, Max Planck Gesellschaft},
 pdfsubject={Presentation at the 2nd MPG-NFDI Workshop on April 18th in Göttingen},
 pdfdate={D:20240418150000Z},
 pdfcreator={Emacs 28.2 (Org mode 9.6.21)}, 
 pdflang={English}}
\input{resources/colors}
\begin{document}

\maketitle
\definecolor{mpievolbio_green}{RGB}{0,124,119}
\setbeamercolor*{frametitle}{fg=white,bg=MPGGreen}
\setbeamercolor*{section in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor*{date in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor*{author in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor*{title in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor{structure}{bg=MPGGreen}
\setbeamercolor{block title}{fg=white,bg=structure.bg}
\setbeamercolor{block body}{use=block title,bg=block title.bg!10}
\setbeamercolor{title}{fg=white,bg=MPGGreen}
\setbeamercolor{itemize item}{fg=MPGGreen}
\setbeamercolor{itemize subitem}{fg=MPGGreen}
\setbeamercolor{enumerate item}{fg=MPGGreen}
\setbeamercolor{enumerate subitem}{fg=MPGGreen}
\setbeamercolor{description item}{fg=MPGGreen}
\setbeamertemplate{itemize item}[circle]
\setbeamertemplate{itemize subitem}[circle]
\setbeamertemplate{itemize subsubitem}[circle]
\setbeamercolor{alerted text}{fg=MPGOrange,bg=MPGLightGreen}

\section{NFDI4BIOIMAGE}
\label{sec:org1e5a62e}
\begin{frame}[label={sec:org5e45126}]{The NFDI4BIOIMAGE Consortium}
\begin{center}
\includegraphics[width=.8\textwidth]{nfdi4bi_taskareas-crop.pdf}
\end{center}
\begin{itemize}
\item Harmonization of bioimage data and metadata  formats and standards
\item FAIR Image Objects
\item Multimodal metadata vocabularies
\item Standard Operating Procedures for FAIR image processing
\item Proliferation of best practices in image data management and processing
\end{itemize}
\end{frame}
\begin{frame}[label={sec:orgcf486c6}]{All Hands Meeting Oct. 2023}
\begin{center}
\includegraphics[width=.8\textwidth]{AHM2023_group.jpg}
\end{center}
\end{frame}

\begin{frame}[label={sec:org722f431}]{No Milestones due yet}
\begin{center}
\includegraphics[width=.8\textwidth]{2024-04-18_05-01-47_screenshot.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:org320f768}]{Outline}
\begin{block}{}
\begin{itemize}
\item FAIRification of image data: 5 star scheme
\item Teaching \& Training material
\item BIDS, ARC, OMERO interfaces
\item Data integration at MPI-EvolBio
\end{itemize}
\end{block}
\end{frame}
\begin{frame}[label={sec:org26574b4}]{FAIRification of Image (meta)data}
\begin{block}{Quantify the FAIRness of containerized Bioimage research objects}
\end{block}
\end{frame}

\begin{frame}[label={sec:org9e7a9d3}]{FAIRification of Image (meta)data}
\begin{block}{5 stars linked open data}
\begin{center}
\includegraphics[width=0.98\textwidth]{2024-04-12_22-32-37_screenshot.png}
\end{center}
\end{block}
\end{frame}
\begin{frame}[label={sec:orgbda2752}]{FAIRification of Image (meta)data}
\begin{block}{5 star bioimage containers}
\begin{center}
\includegraphics[width=.98\textwidth]{2024-04-10_14-38-17_screenshot.png}
\end{center}
\end{block}
\end{frame}

\begin{frame}[label={sec:org5e32d26}]{Zarr is the technical backbone of next generation (bio)image file formats}
\begin{columns}
\begin{column}{.48\columnwidth}
\begin{block}{What is Zarr?}
\begin{itemize}
\item hdf5 like hierarchical layers
\item chunks stored in separate "files" (blobs), linked by json file (vs. monolithic binary blob in hdf5)
\item suitable for object storage
\end{itemize}
\end{block}
\end{column}
\begin{column}{.48\columnwidth}
\begin{block}{}
\begin{center}
\includegraphics[height=.7\textheight]{monolithic-vs-chunked.pdf}
\end{center}
\tiny 10.5281/zenodo.7037679
\end{block}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:org9602702}]{Zarr is the technical backbone of next generation (bio)image file formats}
\begin{columns}
\begin{column}{.48\columnwidth}
\begin{block}{What is Zarr?}
\begin{itemize}
\item IO latency issues
\item NFDI4BI leads community efforts for specification and implementation
\item BioImage Archive (BIA), NASA, Open Geospatial Consortium (OGC) have committed to adopting Zarr as community standard
\end{itemize}
\end{block}
\end{column}

\begin{column}{.48\columnwidth}
\begin{block}{}
\begin{center}
\includegraphics[height=.7\textheight]{fair-reuse.pdf}
\end{center}
\tiny 10.5281/zenodo.7037679
\end{block}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:org9fc9006}]{The RFC (Request For Comments) process}
\begin{block}{}
\begin{itemize}
\item Established Internet Engineering Task Force (IETF) procedure for
\begin{itemize}
\item protocols (HTTP, FTP)
\item best practices (RFC)
\item standards (RDF)
\item file formats (XML, HTML)
\item anything that defines the internet
\end{itemize}
\end{itemize}
\end{block}
\end{frame}
\begin{frame}[label={sec:orgdc05b75}]{}
\begin{columns}
\begin{column}{0.48\columnwidth}
\begin{block}{The RFC process}
\begin{center}
\includegraphics[height=.8\textheight]{rfc-editor-process.png}
\end{center}
\end{block}
\end{column}
\begin{column}{0.48\columnwidth}
\begin{block}{Example: RFC2119}
\begin{center}
\includegraphics[height=.8\textheight]{2024-04-12_22-00-33_screenshot.png}
\end{center}
\end{block}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:org7834fa8}]{Training (TA5)}
\begin{block}{I3D-bio customizable slide decks}
\begin{center}
\includegraphics[width=.8\textwidth]{2024-04-18_05-35-39_screenshot.png}
\end{center}
\end{block}
\end{frame}

\begin{block}{Training (TA5)}
\begin{block}{Jupyter Book + Search Index}
\begin{center}
\includegraphics[width=.8\textwidth]{2024-04-18_05-38-11_screenshot.png}
\end{center}
\end{block}
\end{block}

\begin{frame}[label={sec:orgdaabf24}]{BIDS-ARC-OMERO (TA1, TA3)}
\begin{columns}
\begin{column}{.48\columnwidth}
\begin{block}{ARC (Annotated Research Context, NFDI4Plants)}
\begin{center}
\includegraphics[height=.7\textheight]{2024-04-18_05-09-10_screenshot.png}
\end{center}
\end{block}
\end{column}

\begin{column}{.48\columnwidth}
\begin{block}{OMERO}
\begin{center}
\includegraphics[height=.7\textheight]{2024-04-18_05-19-09_screenshot.png}
\end{center}
\end{block}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:org75a2fc3}]{Many open questions}
\begin{itemize}
\item One-to-one mapping of ARC entities to/from OMERO?
\item What about non-image data in ARC?
\item OMERO as metadata manager on top of ARC?
\item round trip possible?
\item large image files (think TB) in git?
\end{itemize}
\end{frame}
\begin{frame}[label={sec:org88a0ca2}]{Mapping (attempts)}
\begin{center}
\includegraphics[width=\textwidth]{2024-04-18_08-58-17_screenshot.png}
\end{center}
\tiny{DOI:10.5281/zenodo.8349563}
\end{frame}
\begin{frame}[label={sec:orge3eb629}]{Conclusions so far}
\begin{itemize}
\item conversion OMERO \(\rightarrow\) BIDS \(\rightarrow\) ARC seems possible
\item only for already specified measurements (BIDS standard)
\item requires constant reformatting (e.g. fastq to csv)
\item \(\Leftrightarrow\) Support by format specific processing clients
\end{itemize}
\end{frame}

\section{Data integration for Evolutionary Biology}
\label{sec:orga5ce0b8}
\begin{frame}[label={sec:orgb81fae8}]{MPI-EvolBio's activities}
\tiny
\begin{columns}
\begin{column}{0.3\columnwidth}
\begin{block}{Evolutionary Genetics (D. Tautz (Emeritus))}
\begin{itemize}
\item Model organisms: Mus domesticus, mus musculus
\item Behavioural genomics
\item Population genetics
\end{itemize}
\begin{center}
\includegraphics[width=.9\linewidth]{2023-06-19_09-46-11_screenshot.png}
\end{center}
credits: \url{https://www.evolbio.mpg.de/3039130/group\_evolanimalbehpers}
\end{block}
\end{column}

\begin{column}{0.3\columnwidth}
\begin{block}{Microbial Population Biology (P. Rainey)}
\begin{itemize}
\item Model organism: Pseudomonas fluorescens, Bacillus sub.
\item Evolution of communities
\item Host-microbe interactions
\item Genetics
\end{itemize}
\begin{center}
\includegraphics[width=.9\linewidth]{sbw25.png}
\end{center}
credits: Theodosiou (left), Schwarz (middle), Grote (right)
\end{block}
\end{column}

\begin{column}{0.3\columnwidth}
\begin{block}{Theoretical Biology (A. Traulsen)}
\begin{itemize}
\item Population Structure and Game Theory
\item Metaorganisms
\item Cancer Evolution
\end{itemize}
\begin{center}
\includegraphics[width=.9\linewidth]{2023-06-19_10-07-46_screenshot.png}
\end{center}
Hindersin et al, PLOS Comp. Bio (2019) \url{10.1371/journal.pcbi.1004437}
\end{block}
\end{column}
\end{columns}
\end{frame}

\begin{block}{Data Management Policy enforces deposition of \alert{all} research data}
\begin{center}
\includegraphics[width=.8\textwidth]{data_flowchart.pdf}
\end{center}
\end{block}
\begin{frame}[label={sec:org80394c5}]{Multimodal, multidimensional data in experimental evolution research}
\begin{itemize}
\item Wildtype clone and/or genetically modified strain(s)
\item Timelapse microscopy from growing microbial communities
\item Time resolved whole genome / core gene NGS data
\item Transcription profiles
\item Optical density measurements from plaque assays
\item Functional annotations
\end{itemize}
\alert{\alert{Need for integrated analysis of multiomics-multimodal data}}

"Find images, ELN entries for \(\Delta\) mreB strains and annotations for mreB"
\end{frame}

\begin{frame}[label={sec:org0497055}]{Integrating data the SPARQLing way}
\begin{block}{}
\begin{center}
\includegraphics[width=.9\linewidth]{PfluKnowledgeGraph_20240312.png}
\end{center}
\end{block}
\end{frame}
\begin{frame}[label={sec:org3bb6aa5}]{Data integration of internal sources}
\begin{center}
\begin{tabular}{llll}
DB & API & LOD ready? & RDF conversion\\[0pt]
\hline
 &  &  & \\[0pt]
Tripal Genome DB & JSON-LD & yes & SPARQL\\[0pt]
 &  &  & SPARQL-anything\\[0pt]
 &  &  & virtualization\\[0pt]
 &  &  & \alert{JSON to RDF serialization}\\[0pt]
OMERO & JSON-LD & yes & omero-rdf\\[0pt]
OpenBIS & JSON-LD & yes & ??? ("semantic annotation")\\[0pt]
StrainDB & None & no & csv dump \(\rightarrow\) csv2rdf\\[0pt]
\end{tabular}
\end{center}
\end{frame}

\begin{frame}[label={sec:org1a38418}]{Flattening a deeply nested JSON-LD graph}
\begin{columns}
\begin{column}{0.48\columnwidth}
\begin{center}
\includegraphics[width=.9\linewidth]{2024-03-11_14-32-18_screenshot.png}
\end{center}
\end{column}

\begin{column}{.48\columnwidth}
\begin{block}{}
\begin{itemize}
\item rdflib for loading json-ld into graph structure
\item iteratively parses linked graph uris
\item dask for distributed computing
\item serialized to turtle format
\item -> 2.1 million Triples in ca. 12hrs
\end{itemize}
\end{block}
\end{column}
\end{columns}
\end{frame}

\section{GUI SPARQL clients}
\label{sec:orgc63f9b0}
\begin{frame}[label={sec:org46be875},fragile]{SparNatural for visual query generation}
\begin{center}
\includegraphics[width=.8\textwidth]{2024-04-18_06-02-41_screenshot.png}
\end{center}
\end{frame}

\section{Tasks and contributions to NFDI4BI}
\label{sec:orgdb5b441}
\begin{frame}[label={sec:orgf3f76ef}]{Tasks and contributions to NFDI4BI}
\begin{itemize}
\item P.flu SBW25 Knowledge Graph as a Use Case
\item Controlled vocabularies for microbial population biology and evolutionary cell biology
\item Contributions to linked open data software, clients, recommendations
\end{itemize}
\end{frame}
\end{document}
