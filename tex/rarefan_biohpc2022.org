#+OPTIONS: num:nil toc:nil
#+REVEAL_TRANS: None
#+REVEAL_THEME: Solarized
#+Title: RAREFAN: A publicly accessible bioinformatic pipeline on a cloud server - Implementation and limitations
#+subtitle: [[https://doi.org/10.1101/2022.05.22.493013][doi:10.1101/2022.05.22.493013]] (bioRxiv)
#+Author: _Carsten Fortmann-Grote_, Julia Balk, Frederic Bertels
#+Email: carsten.fortmann-grote@evolbio.mpg.de
#+DATE:     GWDG Life Science/Bioinformatics Workshop\\ 2023-01-16
#+DESCRIPTION: 
#+KEYWORDS: 
#+LANGUAGE:  en
#+OPTIONS:   H:2 num:t toc:t \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:https://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+HTML_LINK_UP:
#+HTML_LINK_HOME:
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [bigger]
#+BEAMER_FRAME_LEVEL: 2
#+OPTIONS: H:2
#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)
#+latex_header: \mode<beamer>{\usetheme{Madrid}}

* Introduction: REPINs and RAYTs
** REPINs and RAYTs
*** What are REPINs
    :PROPERTIES:
    :BEAMER_env: block
    :END:
**** 
    :PROPERTIES:
    :BEAMER_col: 0.34 
    :END:
    
  - Short (\tilde 100bp)
  - Nested inverted repeats $\rightarrow$ hairpins
  - Populate intergenic regions
  - Occur 100s of times in bacterial genomes
  - Highly conserved
  - Unclear function
**** 
    :PROPERTIES:
    :BEAMER_col: 0.64
    :END:
    #+ATTR_LaTeX: :width \textwidth
    #+ATTR_LaTeX: :crop
    [[file:/home/grotec/Pictures/rarefan/repins_genomebrowserview-crop.pdf]]
     
** REPINs and RAYTs
*** REPINs are associated with RAYTs
    :PROPERTIES:
    :BEAMER_ENV: block
    :END:
    [[file:/home/grotec/Pictures/rarefan/repins_rayt_blocks-crop.pdf]]

    - RAYTs replicate REPINs across the genome
      
** REPINs and RAYTs
*** What are REPINs
    :PROPERTIES:
    :BEAMER_env: block
    :END:
**** 
    :PROPERTIES:
    :BEAMER_col: 0.34 
    :END:
    
  - Short (\tilde 100bp)
  - Nested inverted repeats $\rightarrow$ hairpins
  - Populate intergenic regions
  - Occur 100s of times in bacterial genomes
  - Highly conserved
**** 
    :PROPERTIES:
    :BEAMER_col: 0.64
    :END:
    #+ATTR_LaTeX: :width .8\textwidth
    #+ATTR_LaTeX: :crop
    [[file:/home/grotec/Pictures/rarefan/repins_in_pflusbw25.png]]



    
** REPIN Analysis as a Service
*** RAREFAN:
- facilitates REPIN identification in a no-code, easily accessible way:
- identifies RAYTs and REPINs in bacterial genomes
- constructs phylogenies of RAYTs and REPINs across bacterial genomes
- analyses RAYT and REPIN population statistics
  
** REPIN Analysis as a Service
*** RAREFAN workflow
    #+ATTR_LaTeX: :width .87\textwidth
[[https://www.biorxiv.org/content/biorxiv/early/2022/11/14/2022.05.22.493013/F2.large.jpg]]

* RAREFAN
** Using the service

*** RAREFAN Homepage http://rarefan.evolbio.mpg.de
#+ATTR_LATEX: :width .8\textwidth
[[file:RAREFAN/2023-01-13_10-15-20_screenshot.png]]

* Using RAREFAN
** Upload one or multiple genomes to be analyzed

#+DOWNLOADED: screenshot @ 2023-01-13 10:17:56
[[file:Using_RAREFAN/2023-01-13_10-17-56_screenshot.png]]

** Using RAREFAN
*** Set run options

#+DOWNLOADED: screenshot @ 2023-01-13 10:18:54
#+ATTR_LaTeX: :height .8\textheight
[[file:Using_RAREFAN/2023-01-13_10-18-54_screenshot.png]]

** Using RAREFAN
*** Monitor run progress
#+DOWNLOADED: screenshot @ 2023-01-13 10:21:05
  #+ATTR_LaTeX: :height .8\textheight
[[file:Using_RAREFAN/2023-01-13_10-21-05_screenshot.png]]

** Using RAREFAN
*** Run and results summary


#+ATTR_LaTeX: :height .8\textheight
[[file:Using_RAREFAN/2023-01-13_10-29-38_screenshot.png]]

** Using RAREFAN
*** Visualize results: RAYT phylogeny

[[file:Using_RAREFAN/2023-01-13_10-24-40_screenshot.png]]

** Using RAREFAN
*** Visualize results: REPIN tree and population size

#+DOWNLOADED: screenshot @ 2023-01-13 10:26:07
[[file:Using_RAREFAN/2023-01-13_10-26-07_screenshot.png]]

** Using RAREFAN
*** Visualize results: Correlation plot

#+DOWNLOADED: screenshot @ 2023-01-13 10:27:24
[[file:Using_RAREFAN/2023-01-13_10-27-24_screenshot.png]]


** Software Stack
- Webserver: apache2
- Web frontend: flask, dropzone.js
- DB Backend: MongoDB
- Scheduling: redis, python-rq
- Bioinformatic backends:
  - java code "RepinEcology" \fullfootcite{Bertels2011, Bertels2022, Bertels2017a}
  - Blast+ \footfullcite{Camacho2009}
  - MCL \footfullcite{VanDongen2008}
  - Andi \footfullcite{Haubold2015}
  - Muscle \footfullcite{Edgar2004}
  - PhyML3 \footfullcite{Guindon2010}
- Plotting: R (ggplot2, shiny)
  
** RAREFAN components
[[file:drawings/rarefan_workflow.pdf]]

** Host server
- GWDG Cloud Server (8 VCPUs, 16GB RAM)
- Debian 10 GNU/Linux
- 1 Redis task executed at a time
- Java VM limited to 10 GB
 
** Job execution timings
*** Wall time for various species and number of CPUs 
    :PROPERTIES:
    :BEAMER_col: 0.49
    :END:
    
**** Wall time vs. number of submitted genomes
    :PROPERTIES:
    :BEAMER_env: block
    :END:
    #+ATTR_LaTeX: :width \textwidth
[[file:~/Pictures/rarefan/wall_time__vs__genomes.png]]

*** 
    :PROPERTIES:
    :BEAMER_col: 0.49
    :END:
**** Wall time per genome and Mbase genome size
    :PROPERTIES:
    :BEAMER_env: block
    :END:
    #+ATTR_LaTeX: :width \textwidth
    #+ATTR_LaTeX: :height .545\textheight
[[file:~/Pictures/rarefan/normalized_wall_time__vs__species.png]]

*** 
#+latex_env: center
$\circa$ 9 sec per Mbase of submitted genome data

** Performance scales poorly with available CPUs
[[file:~/Pictures/rarefan/timings_neiss.png]]


** Outlook
*** 
- Plenty of headspace to improve backend's parallel performance
- Concurrent processing of submitted jobs is currently not possible
- Limited resources may become an issue when userbase increases
$\Rightarrow$ More/better cloud resources?\\
$\Rightarrow$ Options to offload compute intensive parts to HPC/HTC, e.g. via HPC REST-API\\
$\Rightarrow$ Wrap RAREFAN in workflow engine (galaxy, nextflow, ???)

 
** The End
#+Latex_env: center
*Thank you very much!*

