\documentclass[bigger]{beamer}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{glossaries}
\usepackage[style=chem-acs]{biblatex}
\addbibresource{jabref.bib}
\usepackage[english]{babel}
\usepackage{lmodern}
\usepackage[babel=true]{microtype}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{doi}}{}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{DOI}}{}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{url}}{}}
\AtEveryBibitem{\ifentrytype{article}{\clearfield{editor}}{}}
\AtEveryBibitem{\clearfield{pubstate}}

\usepackage{tikz}
\usetikzlibrary{shapes.geometric}
\usepackage{hyperref}
\usepackage{hyperxmp}
\mode<beamer>{\usetheme{Madrid}}
\mode<presentation>
% \mode<article>{\usepackage{fullpage}}

\input{resources/colors}
\definecolor{mpievolbio_green}{RGB}{0,124,119}
\setbeamercolor*{frametitle}{fg=white,bg=mpievolbio_green}
\setbeamercolor*{section in head/foot}{fg=white,bg=mpievolbio_green}
\setbeamercolor*{date in head/foot}{fg=white,bg=mpievolbio_green}
\setbeamercolor*{author in head/foot}{fg=white,bg=mpievolbio_green}
\setbeamercolor*{title in head/foot}{fg=white,bg=mpievolbio_green}
\setbeamercolor{structure}{bg=mpievolbio_green}
\setbeamercolor{block title}{fg=white,bg=structure.bg}
\setbeamercolor{block body}{use=block title,bg=block title.bg!10}
\setbeamercolor{title}{fg=white,bg=mpievolbio_green}
\setbeamercolor{itemize item}{fg=mpievolbio_green}
\setbeamercolor{itemize subitem}{fg=mpievolbio_green}
\setbeamercolor{enumerate item}{fg=mpievolbio_green}
\setbeamercolor{enumerate subitem}{fg=mpievolbio_green}
\setbeamercolor{description item}{fg=mpievolbio_green}
\setbeamertemplate{itemize item}[circle]
\setbeamertemplate{itemize subitem}[circle]
\setbeamertemplate{itemize subsubitem}[circle]
% \usefonttheme{professionalfonts}

\setbeamercolor{alerted text}{%
  fg=mpievolbio_green
}
% Remove navigation symbols
\setbeamertemplate{navigation symbols}{}
% Position logo in lower right corner
\logo{%
  \begin{tikzpicture}[overlay, remember picture]
    \node[left=-0.18cm] at (0,0){
      \includegraphics[width=0.05\textwidth]{mpg_logo_FGgreen_BGalpha.png}
    };
    \end{tikzpicture}
  }
\usetheme{default}
%
% Metadata for header slide
%
\author{Carsten Fortmann-Grote}
\institute[MPI EvolBio]{Max Planck Institute for Evolutionary Biology}

\date{Dec. 9 2022}
\title[Scientific Computing Services]{Scientific Computing Services}
%
% hypersetup
%
\hypersetup{
 pdfauthor={Carsten Fortmann-Grote},
 pdftitle={Scientific Computing Services}
 pdfkeywords={Data management, programming, data analysis, data science},
 pdfsubject={MPI Evolutionary Biology Group Leader Retreat 2022},
 pdfdate={D:20221209141000Z},
 pdfcreator={pdflatex},
 pdfkeywords={computing, data management, electronic lab notebook, openbis, omero, research software engineering},
 pdflang={English}
 }
%
\input{glossary.tex}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  BEGIN DOCUMENT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\begin{frame}
\maketitle
\end{frame}
%
\section*{Outline}
\begin{frame}
  \frametitle<presentation>{Outline}
  \tableofcontents
\end{frame}
%
\section{Services Overview}
%
\begin{frame}%
    \frametitle{Services Overview}%
    \only<3>{%
    \begin{block}{\gls{rsd}}%
        \includegraphics[width=\textwidth]{Screenshot_2022-12-05_16-56-01}\\%
        Teaching \gls{rsd} best practises, one 2-3 day workshop per year (with N. Glynatsi)
    \end{block}%
    }
    \only<1>{%
        \begin{block}{Support for Scientific Code Projects}%
            \begin{center}
            \includegraphics[width=.6\textwidth]{Screenshot_2022-12-02_15-39-17.png}\\%
            Cancer simulation code (with Luca Opasic)
            \end{center}
        \end{block}
        \begin{columns}
            \begin{column}{.5\textwidth}%
                     \begin{itemize}
                        \item Git Version Control
                        \item Automated testing and deployment
                    \end{itemize}
                \end{column}
                \begin{column}{.5\textwidth}
                    \begin{itemize}
                        \item Code peer review (JOSS)
                        \item Live notebooks (Binder)
                    \end{itemize}
        \end{column}%
    \end{columns}
    }
    \only<2>{%
        \begin{block}{Web deployment of data processing pipelines}
            \begin{center}
                \includegraphics[width=.6\textwidth]{Screenshot_2022-12-02_15-41-44}\\%
                REPIN and RAYT webservice (with Frederic Bertels)
            \end{center}
        \end{block}%
    }
    \only<4>{%
    \begin{block}{High Performance Computing, Debugging, Optimization}%
        \begin{center}
        \includegraphics[width=.6\textwidth]{Screenshot_2022-12-07_10-40-41.png}\\
        From ``Advanced R'' with Loukas Theodosiou and Kristian Ullrich
        \end{center}
    \end{block}%
    \begin{center}
        \textbf{New}: Data and Computing Consultancy, Thursdays 15h-17h, Villa
    \end{center}
}
\only<5>{
    \begin{block}{Data analysis, visualization}%
        \begin{center}
        \includegraphics[width=.6\textwidth]{Screenshot_2022-12-02_16-29-34}\\%
        Distance matrix for different regions in \textit{Pseudomonas fluorescens} genomes (with Pabitra Nandy)
        \end{center}
    \end{block}%
}
\only<6>{
    \begin{block}{Data management}%
        \begin{columns}
            \begin{column}{.33\textwidth}
                \begin{center}
                    \includegraphics[width=.8\textwidth, viewport=50 100 300 300, clip]{Datasources_at_MPIEB.pdf}\\[-1ex]
                    {\tiny (C) Zeiss}\\
                    \textbf{Image data}
                    \begin{itemize}
                        \item Microscopy
                        \item CT, X-ray
                        \item Cameras
                    \end{itemize}
                \end{center}
                \vspace*{\fill}
            \end{column}
            \begin{column}{.33\textwidth}
                \begin{center}
                    \includegraphics[width=.8\textwidth, viewport=330 120 550 300, clip]{Datasources_at_MPIEB.pdf}\\[-1ex]
                    {\tiny (C) Illumina}\\
                    \textbf{Sequence data}
                    \begin{itemize}
                        \item Inhouse
                        \item Offsite 
                    \end{itemize}
                \end{center}
                \vspace*{\fill}
            \end{column}
            \begin{column}{.33\textwidth}
                \begin{center}
                    \includegraphics[width=.8\textwidth, viewport=580 130 800 300, clip]{Datasources_at_MPIEB.pdf}\\[-1ex]
                    {\tiny (C) IBM}\\
                    \textbf{Simulation \& Modeling data}
                    \begin{itemize}
                        \item Onsite: Wallace, Ada
                        \item Offsite: GWDG, MPCDF
                    \end{itemize}
                \end{center}
                \vspace*{\fill}
            \end{column}
        \end{columns}
    \end{block}%
}
\end{frame}
%
\section{Data Management: Current status and open issues}
\begin{frame}%
    \frametitle{The Push for FAIR Data Principles}%
    \begin{itemize}
        \item Data must be Findable, Accessible, Interoperable, and Reproducible
        \item FAIR data principles have become integral part of “Good Scientific Practice” implementation guidelines at
            major funding agencies (DFG, Helmholtz, BMBF, \textbf{MPG})
            \includegraphics[width=\linewidth]{Screenshot_2022-12-05_16-40-11}\\
            {\tiny\url{https://www.mpg.de/197494/rulesScientificPractice.pdf} (06/2021)}
        \item FAIR data management is required by publishers, gouvernments, national and international science organizations.
        \item MPI EvolBio contributes to implementation of FAIR through participation in \textbf{Nationale
            Forschungsdaten Initiative} (NFDI4BioImage) starting 03/2023
    \end{itemize}
\end{frame}%
%
\begin{frame}%
    \frametitle{Image data: OMERO Database}%
    \includegraphics[width=\textwidth, viewport=0 50 800 540, clip]{omero_at_mpieb.pdf}
\end{frame}%
%
\begin{frame}%
    \frametitle{Sequencing data: Current state}%
    \only<1>{%
        \begin{center}
            \includegraphics[width=.9\textwidth]{sequencing_workflow.png}\\
        {\tiny CC-BY (C) BioIcons}
        \end{center}
    }
    \only<2>{%
        \begin{itemize}
            \item Established system, direct communication on personal level
            \item Data spread over multiple locations
            \item Missing feedback on job status
            \item Difficult to locate sequence data for a given project
            \item \texttt{/archive} subdirectories subject to change
            \item Data archiving not documented transparently
        \end{itemize}
    }%
    \only<3>{%
        \begin{description}
            \item[F] 
                To locate sequence data + metadata,  must know the Sequencer Run number AND the sequence lab run number.
                Depends on the PI if all this information is recorded (where) and made accessible.\\
            \item[A] Only if still on hard disk storage\\
                Delayed if written to tape\\
            \item[I] fastq format is a standard format
                Must collect metadata from various resources to properly interpret the data\\
            \item[R] To the extent that additional information is found
        \end{description}
    }%
\end{frame}
%
\begin{frame}%
    \frametitle{Recently in a chatroom\dots}
    \begin{columns}%
        \begin{column}{.49\textwidth}%
            \only<1->{%
        \scriptsize

        \color{red}{
@ME\\
hi XXX, did you have the chance to collect\\
the missing information for the DNA kits?
}

\dots\\

\color{blue}{
@XXX\\
I have not done this yet, because I am trying\\
to to find where the sequences are in the archive\\
folder

I guess there is no hope that YYY know

once I do this I can refer safely to ZZZ\\
}


\color{red}{
@ME\\
do you have a run number?\\
}


\color{blue}{
@XXX\\
YYY should definetely have it in an email

at the moment I am scanning my lab book

\dots it should be in my lab book for sure
}

\dots
}
\end{column}
\begin{column}{.49\textwidth}
    \only<2->{%
    \begin{itemize}
        \item Essential information for a publication is missing
            \pause
        \item Missing information is in the NGS run metadata
            \pause
        \item Labbook is not searchable
            \pause
        \item It should not take 3 people to retrieve the information
            \pause
        \item Had to browse through various directories, spreadsheets and peoples memory to finally get
            the missing information
    \end{itemize}
}
    % \begin{center}
    %     \includegraphics[width=.8\textwidth]{Screenshot_2022-12-06_16-39-13}
    % \end{center}
        \end{column}
    \end{columns}
\end{frame}
%
\begin{frame}%
    \frametitle{Suggestions for improved Data Management}%
    \begin{itemize}
        \item Develop and enforce \textbf{data management guidelines}
        \item Implementation and Adoption of institute wide\\ \textbf{\gls{eln}} and \textbf{\gls{lims}}
            \begin{itemize}
                \item Enhanced by sequence order system
                \item Interfaced to OMERO Image DB
            \end{itemize}
        \item Training 
    \end{itemize}
\end{frame}%
%
\section{Electronic Lab Notebooks}
\subsection*{Current opinions on ELNs}
\begin{frame}%
    \frametitle{Some thoughts on ELNs}%
%     \only<4>{
%         \begin{block}{\href{https://www.laborjournal.de/epaper/LJ_18_03.pdf}{Laborjournal 03/2018}}%
%             \begin{center}
%                     \includegraphics[width=.8\textwidth]{screenshot_laborjournal_eln_titel.png}\\
%                     \includegraphics[width=.49\textwidth]{screenshot_laborjournal_eln_quote-01.png}
%                     \includegraphics[width=.49\textwidth]{Screenshot_2022-12-06_15-02-05.png}
%             \end{center}
%         \end{block}%
%     }
\only<3>{
    \begin{block}{ZBMed ELN Guide (10/2019)\hfill{\tiny\href{https://doi.org/10.4126/FRL01-006415715}{DOI:10.4126/FRL01-006415715}}}%
        \begin{center}
            \includegraphics[width=.8\textwidth, clip, viewport=0 330 1100 500]{zbmed_eln_wegweiser_titelseite}\\
            \includegraphics[width=.8\textwidth]{screenshot_zbmed_eln}
        \end{center}
        \end{block}%
    }
    \only<2>{%
        \begin{block}{\href{https://www.mpg.de/18156413/leitplancken.pdf}{MPG Leitplancken (2022)}}%
        \begin{center}
            \includegraphics[width=.8\textwidth]{leitplancken_avoid_messy_labbook.png}\\
            {\tiny }
        \end{center}
        \end{block}%
    }
    \only<1>{%
                \begin{block}{ELNs \dots}%
        \begin{itemize}
                   \item Are complementary to paper notebooks
                \begin{itemize}
                    \item What is on paper is there for good
                    \item Authenticity may be easier to establish on written paper 
                    \item Handwriting $\Leftrightarrow$ Thinking~\cite{Shibata2018,Ikegami2014}
                \end{itemize}
            \item Facilitate integration of electronic resources in the documentation
                \begin{itemize}
                    \item Spreadsheets
                    \item Images
                    \item Scripts
                    \item Data
                    \item Database cross references
                \end{itemize}
            \item Enables sharing of data and results with collaborators \& supervisors
            \item May simplify meeting shareholder requirements for FAIR Data Management
        \end{itemize}
                \end{block}%
    }
\end{frame}%
\subsection*{OpenBIS}
\begin{frame}%
    \frametitle{Our service: OpenBIS}%
    \begin{block}{OpenBIS combines ELN and LIMS in a single web app}%
        \only<1>{\includegraphics[width=\linewidth]{openbis_in_a_nutshell.png}}%
        \only<2>{\includegraphics[width=\linewidth]{openbis_features.png}}%
    \end{block}%
\end{frame}%
%
\begin{frame}%
    \frametitle{OpenBIS}%
    \begin{block}{Materials collections in OpenBIS\hfill{\tiny Example from openbis.ch demo server}}%
        \begin{center}
            \includegraphics[width=\textwidth]{openbis_screenshot_plasmids.png}
        \end{center}
    \end{block}%
    
\end{frame}%
%
% \begin{frame}%
%     \frametitle{Alternative: LabFolder (centrally hosted by MPDL)}%
%     \begin{block}{\url{https://labfolder.mpdl.mpg.de}}%
%             \begin{center}
%                 \includegraphics[width=.8\textwidth]{Screenshot_2022-12-06_11-59-50}
%             \end{center}
%     \end{block}%
% \end{frame}%
%

%
\begin{frame}%
    \frametitle{OpenBIS @ MPI EvolBio}%
    \begin{block}{}%
        \begin{itemize}
            \item Currently 4 isolated OpenBIS instances
                \begin{itemize}
                    \item[+] No access management required
                    \item[-] No sharing of data and resources
                \end{itemize}
            \item Transitioning to a institute wide OpenBIS instance with multigroup functionality
            \item[$\Rightarrow$] Will require some user administration by PIs
            \item Training and instructions will be provided by IT and Sc. Comp.
            \item Per--group customizations possible
            \item Custom features:
                \begin{itemize}
                    \item Sequence Ordering System
                    \item GMO Reporting
                \end{itemize}
        \end{itemize}
    \end{block}%
\end{frame}%
%
\subsection*{Sequence Ordering}
\begin{frame}
    \frametitle{Sequence ordering in OpenBIS}
    \begin{minipage}{1.0\linewidth}
        \begin{minipage}[t]{.48\linewidth}
            \begin{itemize}
                \footnotesize
                \item<1-> Select \emph{Inventory}/\emph{Seqorder}/\emph{Ngs
                    Orders}/\emph{NGS Order Submissions}. Click on ``New Ngs Request''.
                \item<2-> Select template from dropdown
                \item<3-> Fill out the web form with all information you have at hand:
                    \begin{itemize}
                        \footnotesize
                        \item<4-> Name and Description
                        \item<5->
                            Select \emph{Order Status}: ``01 - Unsubmitted''
                        \item<6->
                            Enter the sample sheet data into the spreadsheet template.
                        \item<7->
                            Tick ``Library prep requested'' if applicable.
                        \item<7->
                            Add notes for sequencing lab staff.
                    \end{itemize}
            \end{itemize}
        \end{minipage}%
        \hfill%
        \begin{minipage}[t]{.49\linewidth}
            \only<1>  {%
                \vspace*{\fill}%
                \includegraphics[width=\textwidth]{Screenshot_2022-08-22_16-21-15.png}%
                \vspace*{\fill}
            }
            \only<2>  {
                \vspace*{\fill}
                \includegraphics[width=\textwidth    ]{Screenshot_2022-12-02_12-26-40.png}
                \vspace*{\fill}
            }
            \only<3-> {
                \vspace*{\fill}
                \includegraphics[height=.8\textheight]{Screenshot_2022-12-02_12-46-02.png}
                \vspace*{\fill}
            }
        \end{minipage}
    \end{minipage}
\end{frame}
\begin{frame}%
    \frametitle{Sequence order tracking in OpenBIS}%
    \only<1>{%
    \begin{block}{Orders can be edited while Status is ``Unsubmitted''}%
        \centering
        \includegraphics[width=\textwidth]{Screenshot_2022-12-02_12-52-10}
    \end{block}%
    }%
    \only<2>{%
    \begin{block}{Active Orders are handled by Sequencing Lab}%
        \centering
        \includegraphics[width=\textwidth]{Screenshot_2022-12-02_14-37-57}
    \end{block}%
    }%
    \only<3>{%
    \begin{block}{Persistent comments facilitate communication among all involved parties}%
        \centering
        \includegraphics[height=.7\textheight]{Screenshot_2022-12-02_14-51-16}
    \end{block}%
    }%
    \only<4>{%
    \begin{block}{Complete runs linked to run directory on \texttt{archive/}}%
        \centering
        \includegraphics[width=\textwidth]{Screenshot_2022-12-02_14-47-47}
    \end{block}%
    }%
\end{frame}%
%
\begin{frame}%
    \frametitle{Copy complete sequence orders to personal Lab Notebook}%
    \begin{block}{}%
        \includegraphics[width=\textwidth]{Screenshot_2022-12-02_14-56-11}
    \end{block}%
    \begin{itemize}
        \item Document data provenance
        \item Samples $\Leftrightarrow$ Sequencer runs $\Leftrightarrow$ Sequence Data
    \end{itemize}
\end{frame}%
%
\subsection*{GMO reporting}
\begin{frame}
  \frametitle{GMO reporting in OpenBIS}
  \only<1>{%
    \begin{block}{Lab responsibles have to report list of GMOs}

        \includegraphics[width=.8\textwidth]{Screenshot_2022-12-02_14-26-24}
    \end{block}
  }
  \only<2>{%
    \begin{block}{Example: MPB Strain and Plasmid Collections}
      \begin{figure}[h]
        \centering
        \includegraphics[width=\textwidth, clip, viewport=0 300 1500 500 ]{Screenshot_2022-12-01_13-49-18}\\[3ex]
        \includegraphics[width=\textwidth, clip, viewport=0 200 1500 500 ]{Screenshot_2022-12-01_13-51-55}%\\
      \end{figure}
    \end{block}
  }
\end{frame}
\begin{frame}
  \frametitle{Linking Strain and Vector}
  \begin{minipage}[ht]{\linewidth}
    \begin{minipage}[ht]{.4\linewidth}
      \begin{block}{Select vector plasmid as ``Parent''}
        \includegraphics[width=\textwidth]{Screenshot_2022-12-01_14-13-18}
      \end{block}
    \end{minipage}
    \begin{minipage}[ht]{.599\linewidth}
      \begin{itemize}
      \item Pulls vector specific information from the Plasmids collection
      \item Inserts GMO dates into corresponding columns in strain collection
      \item Select GMO columns to generate report 
      \end{itemize}
    \end{minipage}
  \end{minipage}
\end{frame}
\begin{frame}
  \frametitle{GMO reporting in OpenBIS}
  \only<1>{%
    \begin{block}{Merging strain and vector tables generates GMO report}
      \centering
      \includegraphics[width=.79\textwidth]{Screenshot_2022-12-01_13-55-36}
    \end{block}
  }
  \only<2>{%
    \begin{block}{}
      \begin{itemize}
      \item This solution was developed for MPB
      \item Pending some final fixes requested by Lab Manager
      \item Custom (per group) solutions possible
      \end{itemize}
    \end{block}
  }
\end{frame}
% \subsection*{Data Sharing}
% \begin{frame}%
%     \frametitle{SEEK: A FAIR data sharing and commons platform}%
%     \only<1>{%
%     \begin{block}{}%
%         \begin{itemize}
%             \item SEEK offers a solution for
%             \item Structuring data into projects, investigations, studies and assays (ISA Metadata Standard)
%             \item Establishing links between these entities, OMERO and OpenBIS ELN
%             \item Controlling access levels for colleagues, collaborators (external), groups, departments
%             \item Publishing data $\rightarrow$ Accessible on SEEK homepage without login
%             \item Choosing a data license
%             \item DOI assignment
%             \item Tracking data provenance through pipelines and workflows
%             \item Valuable for packaging entire projects for archiving, sharing and supplementing publications
%         \end{itemize}
%     \end{block}%
% }%
%     \only<2>{%
%         \begin{block}{Example SEEK project (PhD project J. Summers)}%
%             \begin{center}
%                 \includegraphics[width=.8\linewidth]{seek_example_js}
%             \end{center}
%         \end{block}%
%     }
% \end{frame}%
%
% \section{Data Management Guidelines}
% %
% \begin{frame}%
%     \frametitle{Data Management Guidelines for MPI EvolBio }%
%     \begin{block}{Summary\hfill{\tiny{[\url{https://gitlab.gwdg.de/mpievolbio-it/data-management-guide/-/wikis/home}]}}}%
%         \begin{enumerate}
%             \item Use the \textbf{OpenBIS} electronic lab notebook (ELN) and laboratory information management system (LIMS) to document your work
%             \item Load your images (microscopy, camera, CT/X-ray, ...) to the \textbf{OMERO} image database.
%             \item Order inhouse sequencing jobs through OpenBIS.
%             % \item Document your project structure in \textbf{SEEK} and cross link to image and sequence data.
%             % \item Grant access to your data to collaborators using SEEK.
%         \end{enumerate}
%     \end{block}%
%     \begin{center}
%     Comments welcome!!!
%     \end{center}
% \end{frame}%
\section{Summary and Conclusions}
%
\begin{frame}%
    \frametitle{Summary and Conclusions}%
        \begin{itemize}
            \item Adherence to FAIR Data Principles is part of Good Scientific Practice
            \item Scientific Computing offers various data and computing services including training in data and
                software literacy
            \item ELN/LIMS systems offer an opportunity to meet such requirements
            \item IT \& Scientific Computing offer the OpenBIS ELN\&LIMS system
            \item OpenBIS extensions for sequence ordering and GMO reporting
        \end{itemize}
\end{frame}%
\begin{frame}%
    \frametitle{Outlook}%
    \begin{itemize}
        \item Rolling out institute wide OpenBIS instance
        \item Transfer of existing data
        \item Finalization and \textbf{testing} of sequence order system
        \item Data management guidelines
        \item User and admin training
    \end{itemize}
\end{frame}%
%
\begin{frame}[allowframebreaks]
  \frametitle<presentation>{References}
  \printbibliography
\end{frame}
\end{document}
