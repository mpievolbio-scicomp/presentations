\documentclass[bigger]{beamer}
\usepackage{tikz}
\usetikzlibrary{positioning}
\usepackage{calc,amsmath,amssymb,amsfonts}
\usepackage{nameref}
\usepackage[sfdefault]{roboto}
\usepackage[LGR,T1]{fontenc}
\usepackage[english]{babel}
\usepackage[style=authoryear-icomp, backend=biber]{biblatex}
\usepackage{array,supertabular,hhline}
\usepackage[]{graphicx}
\usepackage{marvosym}
\usepackage{hyperref}
\usepackage[numberedsection=nameref]{glossaries}
\usepackage{hyperxmp}

\mode<beamer>{\usetheme{Madrid}}
\usetheme{default}
\input{resources/colors}
\definecolor{mpievolbio_green}{RGB}{0,124,119}
\setbeamercolor*{frametitle}{fg=white,bg=MPGGreen}
\setbeamercolor*{section in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor*{date in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor*{author in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor*{title in head/foot}{fg=white,bg=MPGGreen}
\setbeamercolor{structure}{bg=MPGGreen}
\setbeamercolor{block title}{fg=white,bg=structure.bg}
\setbeamercolor{block body}{use=block title,bg=block title.bg!10}
\setbeamercolor{title}{fg=white,bg=MPGGreen}
\setbeamercolor{itemize item}{fg=MPGDarkGreen}
\setbeamercolor{itemize subitem}{bg=MPGDarkGreen}
\setbeamercolor{enumerate item}{bg=MPGGreen}
\setbeamercolor{enumerate subitem}{bg=MPGGreen}
\setbeamercolor{description item}{bg=MPGGreen}
\setbeamercolor{alerted text}{fg=MPGOrange,bg=MPGLightGreen}
\titlegraphic{\includegraphics[height=0.3\textheight]{Luftbild.jpg}}
\logo{\includegraphics[width=0.05\textwidth]{CC-BY-4.0_88x31.png}\hspace{.02\textwidth}\includegraphics[width=0.05\textwidth]{mpg_logo_FGgreen_BGalpha}}

\AtBeginSection[]
{%
  \begin{frame}%
    \begin{center}%
      \huge%
      \textsc{\insertsection}%
    \end{center}%
  \end{frame}%
}

\date{April 24 2024}
\title{Data Policy}
\hypersetup{
  pdfauthor={Carsten Fortmann-Grote},
  pdftitle={Data Policy},
  pdfsubject={MPB Department Seminar},
  pdfkeywords={good scientific practise, research data management, openbis, omero, publications, library},
  pdfdate={D:20240424090000Z},
  pdflang={English}
}


\addbibresource{jabref.bib}
\makeglossaries
\input{glossary.tex}

\setlength\tabcolsep{1mm}
\renewcommand\arraystretch{1.3}
\author[C. Fortmann-Grote]{\underline{Carsten Fortmann--Grote},
        Beate Gericke,
        Iben Martinsen,\\
        Kristian Ullrich,
        Derk Wachsmuth
}

\begin{document}
\maketitle
% \subsection*{Outline} \begin{frame}% \frametitle{\insertsubsection} \tableofcontents{} \end{frame}
%
\subsection*{Motivation}
\begin{frame}{\insertsubsection}
  \begin{block}{}
    \begin{itemize}
    \item Several recent cases where data was not accessible
    \item[\MVRightarrow{}] Waste of time and resources, papers retracted
      \pause
    \item Something has to change
    \end{itemize}
  \end{block}
\end{frame}
%
\begin{frame}{Enabling change}
  \begin{center}
    \includegraphics[width=\textwidth]{COS_pyramid.pdf}
  \end{center}
\end{frame}
\section{Data Policy}
\subsection*{Principles}
\begin{frame}
  \frametitle{\insertsubsection}
  \begin{itemize}
    \item Procedures to start and end a research project \MVRightarrow{} Data Management Plan
    \item All \textbf{raw data is deposited and documented in internal databases}
      (OpenBIS, OMERO, GitLab)
    \item Raw data and procedures (code) for \textbf{published results deposited in open repositories}
  \end{itemize}
\end{frame}
%
\subsection*{Types of data}
\begin{frame}
  \frametitle{\insertsubsection}
  \begin{block}{Data management}%
    \begin{columns}
      \begin{column}{.33\textwidth}
        \begin{center}
          \includegraphics[width=.8\textwidth, viewport=50 100 300 300, clip]{Datasources_at_MPIEB.pdf}\\[-1ex]
          {\tiny (C) Zeiss}\\
          \textbf{Image data}
          \begin{itemize}
          \item Microscopy
          \item CT, X-ray
          \item Cameras
          \end{itemize}
        \end{center}
        \vspace*{\fill}
      \end{column}
      \begin{column}{.33\textwidth}
        \begin{center}
          \includegraphics[width=.8\textwidth, viewport=330 120 550 300, clip]{Datasources_at_MPIEB.pdf}\\[-1ex]
          {\tiny (C) Illumina}\\
          \textbf{Sequence data}
          \begin{itemize}
          \item Inhouse
          \item Offsite
          \end{itemize}
        \end{center}
        \vspace*{\fill}
      \end{column}
      \begin{column}{.33\textwidth}
        \begin{center}
          \includegraphics[width=.8\textwidth, viewport=580 130 800 300, clip]{Datasources_at_MPIEB.pdf}\\[-1ex]
          {\tiny (C) IBM}\\
          \textbf{Simulation \& Modeling data}
          \begin{itemize}
          \item Onsite: Wallace, Ada
          \item Offsite: GWDG, MPCDF
          \end{itemize}
        \end{center}
        \vspace*{\fill}
      \end{column}
    \end{columns}
  \end{block}%
\end{frame}
%
\subsection*{In a nutshell}
\begin{frame}
  \frametitle{\insertsubsection}
  \begin{tabular}{l|l|l|l|}
    &\textbf{Doc}&\textbf{Int. storage}&\textbf{Pub. storage}
    \\ \hline \textbf{Sequence data} & OpenBIS & /archive & SRA, ENA
    \\ \textbf{Image data}    & OMERO   & OMERO    & BioimageArchive
    \\ \textbf{Code}          & GitLab  & GitLab   & GitHub + edmond
  \end{tabular}
\end{frame}
\section{Data Management Plans}
%
%
\subsection*{Data Management Plan (DMP)}
\begin{frame}
  \frametitle{\insertsubsection}
  \begin{block}{At project start, define
}    \begin{itemize}
    \item What kind of data will be collected (sequence, image, ???)
    \item How much data (approximate total volume)
    \item How will it be stored (central storage, backup, database)
    \item Who has access (PI, group members, staff)
    \item How will it be processed (bioinformatics, image analysis)
    \item How will it be published (open access, data repository)
    \item Data protection requirements (personal data, embargo periods)
    \end{itemize}
  \end{block}
  \only<2>{%
    \begin{alertblock}{Starts with the next PostDoc/PhD student}
    \end{alertblock}
  }
\end{frame}
%
\subsection{Data Management Workflow}
\begin{frame}
  \frametitle{\insertsubsection}
  \begin{center}
    \includegraphics[width=.8\textwidth, clip]{data_flowchart.pdf}\\
  \end{center}
{\tiny
\sloppy
DMP=Data Management Plan\hspace{3ex} FDK=Research Data Coordinator
}
\end{frame}
\section{OpenBIS}
\subsection*{OpenBIS}
\begin{frame}%
  \frametitle{\insertsubsection}%
  \only<1>{\begin{block}{Electronic Lab Notes}%
    \begin{center}
      \includegraphics[width=\linewidth]{Screenshot_2024-04-23_21-01-28.png}
    \end{center}
  \end{block}}%
  \only<2>{\begin{block}{Lab items: Protocols, Chemicals, \textbf{Strains}, Plasmids}
    \begin{center}
        \includegraphics[width=\linewidth]{Screenshot_2024-04-23_21-05-04.png}
    \end{center}
  \end{block}}%
  \only<3>{\begin{block}{Genetically modified organisms table}
      \begin{center}
        \includegraphics[width=\linewidth]{Screenshot_2024-04-23_21-10-57}
      \end{center}
  \end{block}}%
  \only<4>{\begin{block}{Sequence ordering}
      \begin{center}
        \includegraphics[width=\linewidth]{Screenshot_2022-12-02_14-56-11}
      \end{center}
  \end{block}}
\end{frame}%
      % 
\section{OMERO}
%
%
\subsection*{OMERO.web}
\begin{frame}
  \frametitle{\insertsubsection}
  \begin{block}{Web interface structure}
    \begin{center}
      \includegraphics[width=.9\textwidth, clip]{OMERO_web_structure-crop.pdf}
    \end{center}
  \end{block}
\end{frame}
\subsection*{OMERO.import}
\begin{frame}
  \frametitle{\insertsubsection}
  \only<1>{%
  \begin{block}{Select data to import}
    \begin{center}
        \includegraphics[width=.9\textwidth, clip]{OMERO_importer_select.png}
    \end{center}
  \end{block}
  }%
  \only<2>{%
  \begin{block}{Run import queue}
    \begin{center}
        \includegraphics[width=.9\textwidth, clip]{OMERO_importer_queue.png}
    \end{center}
  \end{block}%
  }%
\end{frame}
\subsection*{OMERO.forms}
\begin{frame}%
    \frametitle{\insertsubsection}%
    \begin{block}{Enter metadata into web form}
      \begin{center}
        \includegraphics[height=.7\textheight, clip]{OMERO_forms.png}
      \end{center}
    \end{block}
\end{frame}%
%
\subsection*{OMERO spreadsheet annotation}
\begin{frame}%
    \frametitle{\insertsubsection}%
    \begin{block}{Bulk annotation with spreadsheet}
      \begin{itemize}
      \item Copy image filenames into spreadsheet (column ``Image'')
      \item Add image specific metadata
      \item Upload spreadsheet to OMERO \MVRightArrow{} extracts columns to
        key-value pairs
      \end{itemize}
      \begin{center}
        \includegraphics[width=\textwidth, clip]{OMERO_spreadsheet_annotation.png}
      \end{center}
    \end{block}
\end{frame}%
%
\begin{frame}%
    \frametitle{\insertsubsection}%
        \begin{block}{}
          \begin{center}
            \includegraphics[width=.67\textwidth, clip]{OMERO_annotation_workflow.png}
          \end{center}
        \end{block}
  \end{frame}%
%
\section{Some thoughts on an open science culture}
\begin{frame}
  \begin{center}
    \includegraphics[width=\textwidth]{COS_pyramid_annotated.png}
  \end{center}
\end{frame}
\begin{frame}
  \frametitle{Community}
  \begin{itemize}
    \item Onboarding, offboarding
    \item Open Science Day (with CAU)
  \end{itemize}
\end{frame}
\begin{frame}
  \frametitle{Incentives}
  \begin{itemize}
    \item Open Science Award (Aquavit)
    \item Assess and honor open science in TAC meetings
  \end{itemize}
\end{frame}
\end{document}
