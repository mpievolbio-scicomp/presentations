#+OPTIONS: num:nil toc:nil
#+REVEAL_TRANS: None
#+REVEAL_THEME: Solarized
#+Title: Detecting Fabricated Data
#+subtitle: 
#+Author: _Carsten Fortmann-Grote_
#+Email: carsten.fortmann-grote@evolbio.mpg.de
#+DATE:     MPB Lab Seminar\\ March 1st 2023
#+DESCRIPTION: 
#+KEYWORDS: 
#+LANGUAGE:  en
#+OPTIONS:   H:2 num:t toc:t \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:https://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+HTML_LINK_UP
#+HTML_LINK_HOME:
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [bigger]
#+BEAMER_FRAME_LEVEL: 2
#+OPTIONS: H:2
#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)
#+latex_header: \usepackage{biblatex}
#+latex_header: \mode<beamer>{\usetheme{Madrid}}

* Introduction
** FFP: Fabrication, Falsification, and Plagiarism
*** Fabrication
Presentation of facts or unproven data
*** Falsification
Alteration or selection of certain data to obtain desired results
*** Plagiarism
Presentation of someone else's data, figures, narrative as my own without
proper citation and referencing.

Fabrication and Falsification are often mixed up and used interchangably

** Detection of falsified and/or fabricated data
*** Digit preference methods
- Leading digits frequencies (data spans multiple orders of magnitude)
- Last digit frequencies (mainly human recorded data)
*** Statistical methods
- Variance checks
- Distribution checks
** Which data is fake?
#+DOWNLOADED: screenshot @ 2023-01-27 11:43:33
#+latex_env: figure
#+CAPTION: Reproduced from \cite{Fewster2009}
[[file:Introduction:_The_scientific_reproducibility_crisis/2023-01-27_11-43-33_screenshot.png]]


* Methods to detect forged tabular data
** Benford's law test
Benford's law, also known as "first digit law", states that the leading
digit in numerical datasets is distributed as

\[
    p(D_1=d_1) = \log_{10} \left\{1 + \frac{1}{d} \right\}
\]

#+begin_src python :session :results file :exports results
  import numpy
  import matplotlib.pyplot as plt
  x=numpy.arange(1,10)
  y = numpy.log10(1.+1./x)

  fname = 'benford_law_bars.png'
  fig, ax = plt.subplots(1,1)
  ax.plot(x,y, 'o')
  fig.savefig(fname) 

  fname
#+end_src

#+RESULTS:
[[file:benford_law_bars.png]]


** Generalization to $k$ digits
*** Hill (1995)
\[
    p(D_1=d_1, D_2=d_2, ..., D_k=d_k) = \log_{10} \left\{1 + (d_1d_2...d_k)^{-1}\right\}
\]

*** Example:
Probability for observing the first and second digit to be $32$ is:
\[
    p(D_1=3, D_2=2) = \log_{10} \left\{1 + \frac{1}{32}\right\} \simeq 0.01334
\]

** ... which yields the following 'Benford table'

#+DOWNLOADED: screenshot @ 2023-02-02 11:57:24
#+CAPTION: Reproduced from \cite{Diekmann2007}
[[file:Methods_to_detect_forged_tabular_data/2023-02-02_11-57-24_screenshot.png]]


* History of Benford's law
** Newcomb's discovery
In 1881, US astronomer Simon Newcomb found that pages in a book of logarithms are more
heavily worn on pages with numbers that start with 1 than on pages with higher numbers
[Newcomb1881]. He
concluded that numbers starting with 1 must be more abundant than other numbers and derived a mathematical expression for
the probability to observe 1 as a leading digit.

#+ATTR_LaTeX: :width \textwidth
#+CAPTION: S. Newcomb (wikipedia)
[[file:figures/Simon_Newcomb_01.jpg]]
** S. Benford
#+ATTR_LaTeX: :width \textwidth
#+CAPTION: F. Benford (wikipedia)
[[file:figures/Frank_Benford.jpg]]
Newcomb's discovery remained largely unnoticed until General Electric's engineer F. Benford rediscovered it in 1938 \footfullcite{Benford1938}. Benford collected 20000 numbers
from diverse sources such as "... baseball, atomic weights, areas of rivers, numbering of articles in magazines" (wikipedia) to undermine
his "law".

Funnily, the lengths of his datasets have a suspiciousy low frequency in 1 as starting digit.

* Application to tax fraud detection
** Hill (1998)

#+DOWNLOADED: screenshot @ 2023-02-02 14:27:58
#+CAPTION: After Geyer (2004)
#+ATTR_LaTeX: :width \textwidth
[[file:Application_to_tax_fraud_detection/2023-02-02_14-27-58_screenshot.png]]

* Falsification and Fabrication in Scientific Research
** Falsification and Fabrication in Scientific Research
*** Fabrication
/... report of a set, total or partial, of data that does not exist and were invented...
combining, shaping, and/or organizing the elements as a whole for a special purpose./\cite{tbd}
*** Falsification
/Alteration or selection of certain data to obtain the desired results./ \cite{tbd}
** (In)famous examples
*** E. Poehlmann
- published 10 articles containing fabricated data to support his study of metabolic
  changes in women.
- first scientist to be arrested for scientific fraud
- Studies cost US $ 2.9M
  
** (In)famous examples
*** D.P. Han
- Tampered with HIV research data
- Fined 4.5 years in prison and US\$ 7.2M
  
** (In)famous examples
*** W.S. Hwang
- published two landmark papers in Science about human stem-cell resarch, claiming
  sucessful cloning of human stem cells \cite{tbd}
  
** (In)famous examples
*** J.H. Schön
- Became famous for experimental work on organic semiconductors
- "on track for nobel prize"
- competitors were able to show he used the same noise in many different figures
  (e.g. at different temperatures).
- Expert commitee report presents proof for 16 scientific misconducts (whole datasets
  being re-used in different experiments, experimental data produced from mathematical functions.
- Only Schön was found guilty, head of group and co-author was exonerated.
- Schön's Ph.D. revoked, case went all the way to the federar court where the University's decision was confirmed.
-
** Survey's display a frightening willingness to fake data
*** UC Grad Student's \cite{tbd}
15% would be willing to select, omit, fabricate data to win a grant or to publish a paper
*** UC PostDocs \cite{tbd}
7% willing to select or omit data to improve their results
*** AAAS member survey\cite{tbd}
27% found or witnessed research was fabricated, falsified, or plagiarized

** Distribution of FFP across scientific disciplines
\cite{tbd}


* Application to omics data
** Bradshaw (2021)
*** Result
1st, 2nd digit frequencies are excellent features to discriminate
fabricated from real Copy Number Alteration (CNA) data with Machine Learning
*** Generation of (fake) test data
#+ATTR_LaTeX: :width \textwidth
#+CAPTION: Reproduced from Bradshaw (2021) under CC-BY 4.0 License
[[file:~/Pictures/figs_for_talks/pone.0260395.s001.tif][pone.0260395.s001.tif]]
[[file:~/Pictures/figs_for_talks/pone.0260395.s002.tif][pone.0260395.s002.tif]]



** Bradshaw(2021)
*** Machine learning on count data
**** Column header
    :PROPERTIES:
    :BEAMER_col: 0.49 
    :END:
    #+ATTR_LaTeX: :width \textwidth
    #+ATTR_LaTeX: :crop
    [[file:~/Pictures/figs_for_talks/journal.pone.0260395.g002.PNG][bradshaw2021_fig2]]
    
    
**** Column header
    :PROPERTIES:
    :BEAMER_col: 0.49
    :END:
    #+ATTR_LaTeX: :width \textwidth
    #+ATTR_LaTeX: :crop
    [[file:~/Pictures/figs_for_talks/journal.pone.0260395.g003.PNG][bradshaw2021_fig3]]
    
** Accuracy vs. dataset size 
#+DOWNLOADED: screenshot @ 2023-02-02 14:35:20
#+ATTR_LaTeX: :width \textwidth
#+CAPTION: Reproduced from Bradshaw (2021) under CC-BY 4.0 License
[[file:Application_to_omics_data/2023-02-02_14-35-20_screenshot.png]]

** Applicability to other omics data

* Application to RNASeq Data (WIP)

** Benford analysis of MPB RNASeq Data (MPGC Projects 4293, 4994)
*** 1st digit frequencies are perfectly Benford distributed
#+ATTR_LaTeX: :width \textwidth
[[file:Application_to_RNASeq_Data_(WIP)/2023-02-02_20-47-49_screenshot.png]]

** Benford analysis of MPB RNASeq Data (MPGC Projects 4293, 4994)
*** Leading 2 digit frequencies are perfectly Benford distributed
#+ATTR_LaTeX: :width \textwidth
[[file:Application_to_RNASeq_Data_(WIP)/2023-02-02_20-51-05_screenshot.png]]
** Benford testing for each sample

#+DOWNLOADED: screenshot @ 2023-02-06 14:50:05
#+ATTR_LaTex :width \textwidth
[[file:Application_to_RNASeq_Data_(WIP)/2023-02-06_14-50-05_screenshot.png]]
** Generation of a test dataset for ML
*** Randomization
*** Replacement
*** Imputation
* Testing various distribution for their "Benfordness"
** Distribution testing
*** Normal distribution
#+begin_src python :session :results file :exports results
  import numpy
  import math
  from matplotlib import pyplot as plt

  def benford(d):
    return numpy.log10((d+1)/d)

  loc=0
  scale=1

  sample = numpy.random.normal(loc=loc, scale=scale, size=16384)

  # Get leading digits
  n = numpy.floor(numpy.log10(numpy.abs(sample)))

  r = sample / 10.0**n

  leading_digit = numpy.floor(r).astype(int)

  fig, axs = plt.subplots(1,2, figsize=(15,5))

  bins=numpy.arange(-4,4,0.1)
  axs[0].hist(sample, bins=bins,  density=True, label="lognormal d

  fname = f'benford_normal_mu{loc}_sigma{scale}.png'
  fig.savefig(fname)

  fname # return this to org-mode

#+end_src


#+RESULTS:
[[file:benford_normal_mu0_sigma100.png]]

** Distribution testing
*** Lognormal
#+begin_src python :session :results file :exports results
  import numpy
  import math
  from matplotlib import pyplot as plt

  def benford(d):
    return numpy.log10((d+1)/d)

  loc=1.0
  scale=2

  sample = numpy.random.lognormal(mean=loc, sigma=scale, size=16384)

  # Get leading digits
  n = numpy.floor(numpy.log10(numpy.abs(sample)))

  r = sample / 10.0**n

  leading_digit = numpy.floor(r).astype(int)
  fig, axs = plt.subplots(1,2, figsize=(15,5))

  bins = 10**numpy.linspace(-4,4,81)
  axs[0].hist(sample, bins=bins, density=True, label="lognormal distribution")
  axs[0].set_xscale('log')
  axs[0].set_title(f"LogNormal($\mu={loc}, \sigma={scale}$)")

  bins = numpy.arange(1,11)
  axs[1].hist(leading_digit, bins=bins, density=True)
  axs[1].plot(bins[:-1]+0.5, benford(bins[:-1]), "s", label="Benford")
  axs[1].set_title(f"First digit frequencies")

  plt.legend()

  fname = f'benford_lognormal_mu{loc}_sigma{scale}.png'
  fig.savefig(fname)

  fname # return this to org-mode

#+end_src

#+RESULTS:
[[file:benford_lognormal_mu1.0_sigma2.png]]

*** Lognormal
#+begin_src python :session :results file :exports results
  import numpy
  import math
  from matplotlib import pyplot as plt

  def benford(d):
    return numpy.log10((d+1)/d)

  loc=1
  scale=1

  sample = numpy.random.lognormal(mean=loc, sigma=scale, size=16384)

  # Get leading digits
  n = numpy.floor(numpy.log10(numpy.abs(sample)))

  r = sample / 10.0**n

  leading_digit = numpy.floor(r).astype(int)
  fig, axs = plt.subplots(1,2, figsize=(15,5))

  bins = 10**numpy.linspace(-1,2,31)
  axs[0].hist(sample, bins=bins, density=True, label="lognormal distribution")
  axs[0].set_xscale('log')
  axs[0].set_title(f"LogNormal($\mu={loc}, \sigma={scale}$)")

  bins = numpy.arange(1,11)
  axs[1].hist(leading_digit, bins=bins, density=True)
  axs[1].plot(bins[:-1]+0.5, benford(bins[:-1]), "s", label="Benford")
  axs[1].set_title(f"First digit frequencies")

  axs[0].axvline(x=0.1, color='k')
  axs[0].axvline(x=0.2, color='k')
  axs[0].axvline(x=1, color='k')
  axs[0].axvline(x=2, color='k')
  axs[0].axvline(x=10, color='k')
  axs[0].axvline(x=20, color='k')
  plt.legend()

  fname = f'benford_lognormal_mu{loc}_sigma{scale}.png'
  fig.savefig(fname)

  fname # return this to org-mode

#+end_src

#+RESULTS:
[[file:benford_lognormal_mu1_sigma1.png]]

** Distribution testing
*** Poisson
#+begin_src python :session :results file :exports results
  import numpy
  import math
  from matplotlib import pyplot as plt

  def benford(d):
    return numpy.log10((d+1)/d)

  loc=1
  scale=loc

  sample = numpy.random.poisson(lam=loc, size=16384)

  # Get leading digits
  n = numpy.floor(numpy.log10(numpy.abs(sample)))

  r = sample / 10.0**n

  leading_digit = numpy.floor(r).astype(int)
  fig, axs = plt.subplots(1,2, figsize=(15,5))

  axs[0].hist(sample, bins=range(0,10), density=True)
  axs[0].set_title(f"Poisson($\lambda={loc}$)")

  bins = numpy.arange(1,11)
  axs[1].hist(leading_digit, bins=bins, density=True)
  axs[1].plot(bins[:-1]+0.5, benford(bins[:-1]), "s", label="Benford")
  axs[1].set_title(f"First digit frequencies")

  plt.legend()

  fname = f'benford_poisson_l{loc}.png'
  fig.savefig(fname)

  fname # return this to org-mode

#+end_src

#+RESULTS:
[[file:benford_poisson_l1.png]]

** Distribution testing
*** Poisson
#+begin_src python :session :results file :exports results
  import numpy
  import math
  from matplotlib import pyplot as plt

  def benford(d):
    return numpy.log10((d+1)/d)

  loc=5
  scale=loc

  sample = numpy.random.poisson(lam=loc, size=16384)

  # Get leading digits
  n = numpy.floor(numpy.log10(numpy.abs(sample)))

  r = sample / 10.0**n

  leading_digit = numpy.floor(r).astype(int)
  fig, axs = plt.subplots(1,2, figsize=(15,5))

  axs[0].hist(sample, bins=range(0,15), density=True)
  axs[0].set_title(f"Poisson($\lambda={loc}$)")

  bins = numpy.arange(1,11)
  axs[1].hist(leading_digit, bins=bins, density=True)
  axs[1].plot(bins[:-1]+0.5, benford(bins[:-1]), "s", label="Benford")
  axs[1].set_title(f"First digit frequencies")

  plt.legend()

  fname = f'benford_poisson_l{loc}.png'
  fig.savefig(fname)

  fname # return this to org-mode

#+end_src

#+RESULTS:
[[file:benford_poisson_l5.png]]

** Distribution testing
** Distribution testing
*** Poisson
#+begin_src python :session :results file :exports results
  import numpy
  import math
  from matplotlib import pyplot as plt

  def benford(d):
    return numpy.log10((d+1)/d)

  loc=10
  scale=loc

  sample = numpy.random.poisson(lam=loc, size=16384)

  # Get leading digits
  n = numpy.floor(numpy.log10(numpy.abs(sample)))

  r = sample / 10.0**n

  leading_digit = numpy.floor(r).astype(int)
  fig, axs = plt.subplots(1,2, figsize=(15,5))

  axs[0].hist(sample, bins=range(0,20), density=True)
  axs[0].set_title(f"Poisson($\lambda={loc}$)")

  bins = numpy.arange(1,11)
  axs[1].hist(leading_digit, bins=bins, density=True)
  axs[1].plot(bins[:-1]+0.5, benford(bins[:-1]), "s", label="Benford")
  axs[1].set_title(f"First digit frequencies")

  plt.legend()

  fname = f'benford_poisson_l{loc}.png'
  fig.savefig(fname)

  fname # return this to org-mode

#+end_src

#+RESULTS:
[[file:benford_poisson_l10.png]]

** Distribution testing
*** Uniform
#+begin_src python :session :results file :exports results
  import numpy
  import math
  from matplotlib import pyplot as plt

  def benford(d):
    return numpy.log10((d+1)/d)

  loc=-1
  scale=1

  sample = numpy.random.uniform(low=loc, high=scale, size=16384)

  # Get leading digits
  n = numpy.floor(numpy.log10(numpy.abs(sample)))

  r = sample / 10.0**n

  leading_digit = numpy.floor(r).astype(int)

  fig, axs = plt.subplots(1,2, figsize=(15,5))

  axs[0].hist(sample, bins=numpy.linspace(loc, scale, 21), density=True)
  # axs[0].set_title(f"Uniform(${loc},{scale}$)")

  bins = numpy.arange(1,11)
  axs[1].hist(leading_digit, bins=bins, density=True)
  axs[1].plot(bins[:-1]+0.5, benford(bins[:-1]), "s", label="Benford")
  axs[1].set_title(f"First digit frequencies")

  plt.legend()

  fname = f'benford_uniform_{loc}-{scale}.png'
  fig.savefig(fname)

  fname # return this to org-mode

#+end_src

#+RESULTS:
[[file:benford_uniform_-1-1.png]]

** Distribution testing
*** Log-Uniform
#+begin_src python :session :results file :exports results
  import numpy
  import math
  from matplotlib import pyplot as plt

  def benford(d):
    return numpy.log10((d+1)/d)

  loc=-1
  scale=2

  uniform_sample = numpy.random.uniform(low=loc, high=scale, size=16384)
  sample = 10**uniform_sample

  # Get leading digits
  n = numpy.floor(numpy.log10(numpy.abs(sample)))

  r = sample / 10.0**n

  leading_digit = numpy.floor(r).astype(int)

  fig, axs = plt.subplots(1,2, figsize=(15,5))
  axs[0].hist(numpy.log10(sample), density=True)
  axs[0].set_xlabel("Log10(x)")
  axs[0].set_title("LogUniform")

  bins = numpy.arange(1,11)
  axs[1].hist(leading_digit, bins=bins, density=True)
  axs[1].plot(bins[:-1]+0.5, benford(bins[:-1]), "s", label="Benford")
  axs[1].set_title(f"First digit frequencies")

  plt.legend()

  fname = f'benford_loguniform_{loc}-{scale}.png'
  fig.savefig(fname)

  fname # return this to org-mode

#+end_src

#+RESULTS:
[[file:benford_loguniform_-1-2.png]]

#+begin_src python :session :results file :exports results
  import numpy
  import math
  from matplotlib import pyplot as plt

  def benford(d):
    return numpy.log10((d+1)/d)

  loc=-1
  scale=1

  uniform_sample = numpy.random.uniform(low=loc, high=scale, size=16384)
  sample = 10**uniform_sample

  # Get leading digits
  n = numpy.floor(numpy.log10(numpy.abs(sample)))

  r = sample / 10.0**n

  leading_digit = numpy.floor(r).astype(int)

  fig, axs = plt.subplots(1,2, figsize=(15,5))
  axs[0].hist(numpy.log10(sample), density=True)
  axs[0].set_xlabel("Log10(x)")
  axs[0].set_title("LogUniform")

  bins = numpy.arange(1,11)
  axs[1].hist(leading_digit, bins=bins, density=True)
  axs[1].plot(bins[:-1]+0.5, benford(bins[:-1]), "s", label="Benford")
  axs[1].set_title(f"First digit frequencies")

  plt.legend()

  fname = f'benford_loguniform_{loc}-{scale}.png'
  fig.savefig(fname)

  fname # return this to org-mode

#+end_src

#+RESULTS:
[[file:benford_loguniform_-1-1.png]]

*** A criterion for "Benfordness"
The Fourier transform of the distribution in log scale must be zero at non-zero integer frequencies \cite{Smith2003}.
- All discrete non-zero integer frequencies vanish exactly (cropped exponential)
- All frequencies $\leq 1$ vanish (wide Gaussian)
** Diekmann (2007)
*** Even fabricated data exhibit Benford distribution of first digits
- Benford tests should rather focus on later digits
#+DOWNLOADED: screenshot @ 2023-01-27 13:50:02
[[file:Methods_to_detect_forged_tabular_data/2023-01-27_13-50-02_screenshot.png]]

* Last digit methods
** Checking the last digit in hand-recorded data
*** Human's tend to round off to 0 or 5 in the last digit

#+DOWNLOADED: screenshot @ 2023-02-06 11:15:30
#+ATTR_LaTeX: :width \textwidth
#+CAPTION: Lawson (2019)
[[file:Last_digit_methods/2023-02-06_11-15-30_screenshot.png]]

** Distribution testing
*** If the underlying statistical distribution can be assumed, the likelihood
of the presented data for belonging to that distribution can help identifying odd records.

** Lawson (2019)
*** Detection of changes in mean and variance in skin-prick allergy test data
**** Positive control

#+DOWNLOADED: screenshot @ 2023-02-07 09:01:01
[[file:Last_digit_methods/2023-02-07_09-01-01_screenshot.png]]

** Lawson (2019)
*** Detection of changes in mean and variance in skin-prick allergy test data
**** Negative control

#+DOWNLOADED: screenshot @ 2023-02-07 09:02:07
[[file:Last_digit_methods/2023-02-07_09-02-07_screenshot.png]]


* Statistical methods
** Variance checks
/Fraudulent data enterers may
be able to enter data such that mean differences are apparent and make sense within the
confines of a trial; however, it is much less likely that they will be able to fabricate data with
reliable variances./ \cite{Lawson2019}

- Just how much the variance is supposed to change with the mean depends
  on the distribution



* Methods to detect forged image data
* Summary
* Conclusions
** Conclusions
- FFP is a prevalent problem across scientific disciplines
- Faking data is (often) harder than actually doing the research
-  
* Outlook


** Why this talk?
- FFP and the reproducibility crisis are prevalent problems in science
- Acts of FFP are motivated by
  - laziness
  - pressure (conceived or real)
- So what is there to do about it?
  - Staff, supervisors:
    - Create working atmosphere of mutual trust and openness (what do you need?)
    - Technical measures to ease management of your data
    - 
  - Students, PostDocs
    - Act responsibly: 
 
** The End
#+Latex_env: center
*Thank you very much!*


# Local Variables:
# org-babel-python-command: "/opt/anaconda3/bin/python3"
# org-confirm-babel-evaluate: nil
# End:
