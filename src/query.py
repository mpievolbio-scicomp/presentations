#!/usr/bin/env python
# coding: utf-8

from rdflib import Graph, URIRef, Literal
import os

import pandas


query = """
prefix PDF: <http://ns.exiftool.org/PDF/PDF/1.0/> 
prefix prism: <http://ns.exiftool.org/XMP/XMP-prism/1.0/>
prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix xmpdc: <http://ns.exiftool.org/XMP/XMP-dc/1.0/>
prefix xmppdf: <http://ns.exiftool.org/XMP/XMP-pdf/1.0/>
SELECT ?fname ?date ?title ?subject ?keywords ?doi  WHERE {
    ?fname PDF:Title ?title;
       PDF:Subject ?subject.
   optional {
    ?fname prism:DOI ?doi .
    }
    optional {
      ?fname xmppdf:Keywords ?keywords.
    }
    optional {
    ?fname xmpdc:Date ?date. }
}"""

graph = Graph().parse('metadata.rdf.xml')


results = graph.query(query)
df = pandas.DataFrame(data=results)


df.columns = [str(v) for v in results.vars]

df['fname'] = "/" + df['fname'].apply(os.path.basename)
df['doi'] = "<a href=https://dx.doi.org/" + df['doi'] + ">" + df['doi'] + "</a>"
df['title'] = "<a href=/presentations/" + df['fname'] + ">" + df['title'] + "</a>"

df.drop(axis=1, columns='fname').to_json('presentations.json', orient='split')