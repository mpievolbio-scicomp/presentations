# Presentations

This repository contains all presentations by the [Scientific Computing Services](https://www.evolbio.mpg.de/scicomp)
unit at [Max Planck Institute for Evolutionary Biology](https://www.evolbio.mpg.de/). 

A list of presentations with links to the pdf files is at https://mpievolbio-scicomp.pages.gwdg.de/presentations/.

## Installation
### Dependencies
- LaTeX:
  - beamer
  - tikz/pgf
  - latexmk
  - pdflatex
- python:
  - rdflib
  - pandas

* Packages (Debian/GNU Linux, Ubuntu, ...)
- exiftools
- git-lfs
```console
apt update
apt install exiftools git-lfs
```

### LaTeX input
```console
ls -1 tex/*.tex > tex/tex.ls
```

#### Images
```console
git lfs pull
```

#### Bibliography
```console
- curl -o tex/jabref.bib https://owncloud.gwdg.de/index.php/s/XgrhAwv2ECiqEpJ/download
```
#### Glossary
```console
curl -o tex/glossary.tex https://owncloud.gwdg.de/index.php/s/6lpPaJPEU5kYK4b/download
```

## Building
### LaTex
```console
cd tex
for t in $(cat tex.ls); do latexmk $t; done
```

### RDF
```console
exiftool -X *.tex > metadata.rdf.xml
```

## Add included images to repo:
```console
cd src
./gather_images.sh
cd ../tex
git lfs track images/*
git add -A images
```

## License
### Source code
All source code is licensed under BSD-Clause-3 Open Source license. For more information, see LICENSE.

### Documents
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=80/>

Unless otherwise indicated, all documents are licensed under [Creative Commons Attribution Share-Alike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/)



